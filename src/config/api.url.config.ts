const adresse = 'http://localhost:9090/';

export const API_URLS = {
  CIBLE_URLS: adresse + 'api',
  DOMAINES_URLS: adresse + 'domaine/',
  REPRESENTANT_URLS: adresse + 'representant/',
  SousDOMAINE_URLS: adresse + 'sousDomaine/',
  TodoCible: adresse + 'todoCible/',
  EVENEMENT_URL: adresse + 'evenement/',
  ADRESSE: adresse,
}
