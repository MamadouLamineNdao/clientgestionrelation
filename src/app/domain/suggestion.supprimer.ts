import {Suggestion} from "./suggestion";

export class SuggestionSupprimer extends Suggestion{
  public dateSuppression : Date;
  public motif  : string;
}
