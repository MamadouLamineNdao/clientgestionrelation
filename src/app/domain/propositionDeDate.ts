import {DemandeMisEnAttente} from './demandeMisEnAttente';

export class PropositionDeDate {
  public dateDebutValidation: number;
  public dateFinValidation: number;
  public demandeMisEnAttente: DemandeMisEnAttente;
}
