import {Reponse} from './reponse';

export class DemandeAccepte extends Reponse {
  public dateDebutValidation: number;
  public dateFinValidation: number;
}
