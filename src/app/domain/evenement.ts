import {Intermediaire} from './intermediaire';

export class Evenement {

  public idEvenement: number;
  public dateEvenement: number;
  public dateModification: number;
  public dateCreation: number;
  public objectif: string[] = [];
  public lieuEvement: string;
  public intermediaire: Intermediaire ;
  // variable supplementaire qui n'est pas enregistrer et permet de gerer le Type
  public type: string;
}
