import {Evenement} from './evenement';
import {PieceJointe} from './piece-jointe';

export class Reunion extends Evenement {
  public compteRendu: string;
  public ordreDuJour: string [] = [];
  public pieceJointes: PieceJointe[];
}
