import {Partenaire} from './partenaire';
import {PieceJointe} from './piece-jointe';
import {Intermediaire} from './intermediaire';

export class Officiel {
  public idOfficiel: number;
  public dateOfficialisation: number;
  public intermediaire: Intermediaire;
  public pieceJointes: PieceJointe[];
}
