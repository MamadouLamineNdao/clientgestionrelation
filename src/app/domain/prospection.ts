

export class Prospection {
  public  idProspection: number;
  public nomEntreprise: string;
  public secteur: string;
  public taille: string;
  public adresse: string;
  public siteWeb: string;
  public apropos: string;
}
