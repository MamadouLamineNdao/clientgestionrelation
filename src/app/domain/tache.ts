import {Intermediaire} from "./intermediaire";

export class Tache{
  public  idTache : number;
  public  mission : string;
  private  status : boolean;

  public  intermediaire : Intermediaire;
}
