import {Demande} from './demande';

export class Reponse {
  public idReponse: number;
  public commentaire: string;
  public dateReponse: number;
  public demande: Demande;
}
