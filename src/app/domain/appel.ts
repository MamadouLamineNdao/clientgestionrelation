import {Evenement} from "./evenement";
import {Representant} from "./representant";
import {Mail} from "./mail";

export class Appel extends Evenement {
  public nom: string;
  public numero: string;
  public etat: boolean;
  public representant: Representant;
  public resume: string;
  public mails: Mail[];
  public appels: Appel[];
}
