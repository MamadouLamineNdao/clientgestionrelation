import {Mail} from './mail';
import {Reunion} from './reunion';

export class PieceJointe {
  id: number;
  nom: string;
  url: string;
  type: string;
  mail: Mail;
  reunion: Reunion;
}
