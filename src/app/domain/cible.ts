import {Partenaire} from "./partenaire";
import {ElementSuggerer} from "./element.suggerer";

export class Cible {
  public  idCible: number;
  public dateValidationSuggestion: number;
  public codePostal: string;
  public adresse: string;
  public adresseMail: string;
  public telephone: string;


  public  elementSuggerer: ElementSuggerer;

  public  partenaire: Partenaire;

}
