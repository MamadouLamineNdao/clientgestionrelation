import {Evenement} from "./evenement";
import {Evaluation} from "./evaluation";
import {Tache} from "./tache";
import {Partenaire} from "./partenaire";
import {Officiel} from './Officiel';

export class Intermediaire extends Partenaire {
  public dateModification: number;
  public evenements: Evenement[]= [];
  public evaluations: Evaluation[] = [];
  public taches: Tache[] = [];
  public officiel: Officiel;

}
