import {Intermediaire} from "./intermediaire";

export class Evaluation{
  public idEvaluation : number;
  public dateEvaluation : Date;
  public score : number;
  public intermediaire : Intermediaire;
}
