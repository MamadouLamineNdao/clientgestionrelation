import {Reponse} from './reponse';

export class Demande {
  public idDemande: number;
  public type: {};
  public dateDeRedaction: number;
  public dateDeDebut: number;
  public dateDeFin: number;
  public cause: string;
  public annulation: boolean;
  public reponse: Reponse;
}
