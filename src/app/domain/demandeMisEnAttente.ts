import {PropositionDeDate} from './propositionDeDate';
import {Reponse} from './reponse';

export class DemandeMisEnAttente extends Reponse {
  public validation: number;
  public propositionDeDates: PropositionDeDate[] = [];
}
