import {Evenement} from './evenement';
import {Representant} from './representant';
import { PieceJointe} from './piece-jointe';

export class Mail extends Evenement {
  public objetMail: string;
  public pieceJointe: string;
  public contenu: string;
  public representant: Representant;
  public pieceJointes: PieceJointe [];
}
