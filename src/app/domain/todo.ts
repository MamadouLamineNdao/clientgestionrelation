export class Todo {
  id: number ;
  text: string;
  editText: string ;
  isOver: boolean ;
  isEdit: boolean ;
  type: string;
}

