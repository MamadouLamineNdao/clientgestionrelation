import {Entreprise} from "./entreprise";
import {Mail} from "./mail";
import {Appel} from "./appel";

export class Representant {

  public idRepresentant: number;
  public nom: string;
  public prenom: string ;
  public email: string ;
  public tel: string ;
  public fonction: string;
  public appels: Appel[] = [];
  public mails: Mail[] = [];
  public entreprise: Entreprise ;
}
