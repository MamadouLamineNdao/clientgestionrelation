import {Cible} from "./cible";

export class Partenaire {

  public idPartenaire: number;

  public dateDebutPartenariat: number;

  public  cible: Cible;
}
