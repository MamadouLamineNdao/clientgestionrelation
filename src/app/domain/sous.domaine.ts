import {Domaine} from './domaine';
import {Entreprise} from './entreprise';

export class SousDomaine {
  public idSousDomaine: number ;
  public libelle: String ;
  public entreprises: Entreprise[] ;
  public domaine: Domaine ;
  /*attribut supplementair */
  public numberEntreprise: number ;

}
