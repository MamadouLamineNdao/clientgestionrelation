export class Suggestion{
  public idSuggestion : number;
  public nomSuggestion : string;
  public domaine : string;
  public description : string;
  public lieu : string;
}
