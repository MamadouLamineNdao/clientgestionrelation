import {SousDomaine} from './sous.domaine';

export class Domaine {

  public idDomaine: number;
  public nomDomaine: string;
  public sousDomaines: SousDomaine[];
}
