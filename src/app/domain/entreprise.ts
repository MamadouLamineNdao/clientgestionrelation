import {Cible} from './cible';
import {Representant} from './representant';
import {SousDomaine} from './sous.domaine';
import {Taille} from './taille';
import {Officiel} from './Officiel';

export class Entreprise extends Cible {
  public nomEntreprise: string;
  public tailleEntreprise: Taille ;
  public siteInternet: string ;
  public sousDomaines: SousDomaine[] = [] ;
  public representants: Representant[] = [] ;
  // cette variable n'est pas enregistrer
  officiel: Officiel;

}
