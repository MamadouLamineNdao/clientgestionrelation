import {Injectable} from '@angular/core';

import {RootComponent} from '../shared/roots/root.component';
import {GlobalService} from '../shared/services/global.service';


@Injectable()
export class AgendaService extends RootComponent {

  private _loading: boolean;
  private test: boolean;

  constructor(public _globalService: GlobalService) {
    super(_globalService);
  }


  get loading(): boolean {
    return this._loading;
  }

  set loading(value: boolean) {
    this._loading = value;
  }



  set notification(value: boolean) {
    this.test = value;
    if (this.test === true) {
      this.alertMessage(
        {
          type: 'success',
          title: 'Resultats!',
          value: 'Evenement dans 2h de temps'
        }
      );
      this.loading = false;
    } else {
      this.alertMessage(
        {
          type: 'danger',
          title: 'Erreur!',
          value: 'Vous n\'avez pas de connexion internet'
        }
      );
      this.loading = false;
    }

  }
}
