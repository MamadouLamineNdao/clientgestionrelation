import {Evenement} from "../domain/evenement";
import {Appel} from "../domain/appel";
import {Reunion} from "../domain/reunion";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Mail} from "../domain/mail";
import {Partenaire} from "../domain/partenaire";
import {Intermediaire} from "../domain/intermediaire";
import {Injectable} from "@angular/core";
import 'rxjs/Rx';
import {Entreprise} from "../domain/entreprise";
import {Personne} from "../domain/personne";
import {Cible} from "../domain/cible";
import {Representant} from "../domain/representant";
import {API_URLS} from "../../config/api.url.config";

@Injectable()
export class RepresentantService {
  partenaires: Partenaire[] = [];
  evenements: Evenement[] = [];
  intermediaires: Intermediaire[] = [];
  reunions: Reunion[] = [];
  mails: Reunion[] = [];
  baseURL = API_URLS.ADRESSE + 'intermediaire';

  constructor(private http: HttpClient) {
  }

  listeRepresentant(id: number): Observable<Representant[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/' + id + '/representants', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }






  creerRepresentant(id: number , representant: Representant): Observable<Representant> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/' + id + '/representant', representant , {headers: httpHeaders}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }

  updateRepresentant(id: number, representant: Representant): Observable<Representant> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/' + id + '/representant/' + representant.idRepresentant, representant, {headers: httpHeaders}).map(
      event  => representant,
    ).catch(error => Observable.throw(error));
  }


  delete(id: number, representant: Representant): Observable<any> {

    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/' + id + '/representant/' +
      representant.idRepresentant, {headers: httpHeaders, responseType: 'text'}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }


}




