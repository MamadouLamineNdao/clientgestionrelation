import {Evenement} from "../domain/evenement";
import {Appel} from "../domain/appel";
import {Reunion} from "../domain/reunion";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Mail} from "../domain/mail";
import {Partenaire} from "../domain/partenaire";
import {Intermediaire} from "../domain/intermediaire";
import {Injectable} from "@angular/core";
import 'rxjs/Rx';
import {Entreprise} from "../domain/entreprise";
import {Personne} from "../domain/personne";
import {Cible} from "../domain/cible";
import {API_URLS} from "../../config/api.url.config";
import {Officiel} from '../domain/Officiel';

@Injectable()
export class PartenaireService {
  partenaires: Partenaire[] = [];
  evenements: Evenement[] = [];
  intermediaires: Intermediaire[] = [];
  reunions: Reunion[] = [];
  mails: Reunion[] = [];
  baseURL = API_URLS.ADRESSE + 'partenaires';

  constructor(private http: HttpClient) {
  }

  listePartenaire(): Observable<Partenaire[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/all', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  getOfficielByIntermediaire(idIntermediaire): Observable<Officiel> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/officiels/byIntermediaire/' + idIntermediaire, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  listeOfficielEntreprise(): Observable<Officiel[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/intermediaire/officiels/entreprise', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }
  listeOfficielPersonne(): Observable<Officiel[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/intermediaire/officiels/personne', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }
  officialiserPartenaire(officiel: Officiel, idPartenaire: number): Observable<Officiel> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    officiel.dateOfficialisation = new Date().getTime();
    return this.http.post(this.baseURL + '/intermediaire/' + idPartenaire + '/officiel', officiel, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }
  updateOfficiel(officiel: Officiel): Observable<Officiel> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/officiel/' + officiel.idOfficiel, officiel, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  deleteOfficiel(officiel: Officiel): Observable<any> {

    console.log(officiel.idOfficiel);
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/intermediaire/officiel/' +
      officiel.idOfficiel, {headers: httpHeaders, responseType: 'text'}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }

  detailEntrepise( id: number): Observable<Entreprise> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/intermediaire/entreprise/' + id , {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  detailPersonne( id: number): Observable<Personne> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/intermediaire/personne/' + id , {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  listeIntermediaireEntreprise( ): Observable<Intermediaire[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/entreprises', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  listeIntermediairePersonne( ): Observable<Intermediaire[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/personnes', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }




  creerEvenement(even: Evenement): Observable<Evenement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/personnes', even, {headers: httpHeaders}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }

  updateEvenement(even: Evenement): Observable<Evenement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/personnes/' + even.idEvenement, even, {headers: httpHeaders}).map(
      event  => even,
    ).catch(error => Observable.throw(error));
  }
  listeAppel( id: number): Observable<Appel[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/' + id + '/appels' , {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }
  listeReunion( id: number): Observable<Reunion[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/' + id + '/reunions', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  listeMail( id: number): Observable<Mail[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/' + id + '/mails', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  delete(intermediaire: Intermediaire): Observable<any> {

    console.log(intermediaire.idPartenaire);
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/intermediaire/' +
      intermediaire.idPartenaire, {headers: httpHeaders, responseType: 'text'}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }


}




