import {Injectable, ɵConsole} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URLS} from '../../config/api.url.config';
import {Domaine} from '../domain/domaine';

@Injectable()
export class DomaineService {

  constructor(private http: HttpClient) {

  }

  getAllDomaine(): Observable<Domaine[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.DOMAINES_URLS, {headers: httpHeaders}).map(
      domai => domai,
    );
  }
  addDomaine(domaine: Domaine): Observable<Domaine> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(API_URLS.DOMAINES_URLS, domaine, {headers: httpHeaders}).map(
      domai => domai,
    );
  }
  deleteDomaine(domaine: Domaine): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(API_URLS.DOMAINES_URLS + domaine.idDomaine, {headers: httpHeaders, responseType: 'text'}).
      map(
        domai => domai,
      );
  }
  updateDomaine(domaine: Domaine): Observable<Domaine> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(API_URLS.DOMAINES_URLS + domaine.idDomaine, domaine,  {headers: httpHeaders}).
    map(
      domai => domai,
    );
  }
}
