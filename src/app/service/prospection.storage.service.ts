import {Injectable} from '@angular/core';

import {Prospection} from '../domain/prospection';
import {RootComponent} from '../shared/roots/root.component';
import {GlobalService} from '../shared/services/global.service';


@Injectable()
export class ProspectionStorageService extends RootComponent {

  private _prospects: Prospection[];
  private _loading: boolean;

  constructor(public _globalService: GlobalService) {
    super(_globalService);
  }


  get loading(): boolean {
    return this._loading;
  }

  set loading(value: boolean) {
    this._loading = value;
  }

  get prospects(): Prospection[] {
    return this._prospects;
  }

  set prospects(value: Prospection[]) {
    this._prospects = value;
    if (this.prospects !== undefined) {
      this.alertMessage(
        {
          type: 'success',
          title: 'Resultats!',
          value: 'Scrappage terminé : ' + this.prospects.length + ' résultats trouvés'
        }
      );
      this.loading = false;
    } else {
      this.alertMessage(
        {
          type: 'danger',
          title: 'Erreur!',
          value: 'Vous n\'avez pas de connexion internet'
        }
      );
      this.loading = false;
    }

  }
}
