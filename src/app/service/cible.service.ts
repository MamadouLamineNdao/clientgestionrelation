import {Injectable, ɵConsole} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URLS} from '../../config/api.url.config';
import {Personne} from '../domain/personne';
import {Entreprise} from '../domain/entreprise';
import {Cible} from '../domain/cible';
import {Partenaire} from '../domain/partenaire';
import {Intermediaire} from '../domain/intermediaire';
import {Domaine} from '../domain/domaine';
import {Taille} from '../domain/taille';

@Injectable()
export class CibleService {

  personnes: Personne[] = [];
  entreprise: Entreprise[] = [];

  constructor(private http: HttpClient) {

  }


  showPersonnes(): Observable<Personne[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/cible/personnes', {headers: httpHeaders});
  }


  showEntreprises(idSousDomaine: String): Observable<Entreprise[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/cible/entreprises/BySousDomaine/' + idSousDomaine, {headers: httpHeaders});
  }

  findPersonneByNom(nom: string): Observable<Personne[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/cible/entreprise' + nom, {headers: httpHeaders});
  }


  addEntreprise(even: Entreprise): Observable<Entreprise> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    even.dateValidationSuggestion = new Date().getTime();
    return this.http.post(API_URLS.CIBLE_URLS + '/cible/entreprise', even, {headers: httpHeaders}).
    map(entrep => entrep
    )
      ;
  }


  addPersonnes(personne: Personne): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    personne.dateValidationSuggestion = new Date().getTime();
    return this.http.post(API_URLS.CIBLE_URLS + '/cible/personne', personne, {headers: httpHeaders}).map(
      person => person,
    );
  }


  updatePersonne(even: Cible): Observable<Personne> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(API_URLS.CIBLE_URLS + '/cible/personne/' + even.idCible, even, {headers: httpHeaders});
  }

  updateEntreprise(even: Entreprise): Observable<Entreprise> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(API_URLS.CIBLE_URLS + '/cible/entreprise/' + even.idCible, even, {headers: httpHeaders});
  }

  deleteCible(even: Cible): Observable<any> {
    console.log(even) ;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(API_URLS.CIBLE_URLS + '/cible/' + even.idCible, {headers: httpHeaders, responseType: 'text'}).map(
      personne => personne,
    )
      ;
  }

  validateCible(even: Intermediaire): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(API_URLS.CIBLE_URLS + '/cible/validation', even, {headers: httpHeaders}).map(
      partenaire => partenaire
    );
  }
  validateCible2(even: Intermediaire, idCible: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(API_URLS.CIBLE_URLS + '/cible/valider/' + idCible, even, {headers: httpHeaders}).map(
      intermediaire => intermediaire
    );
  }
  counterEntrepriseBydomaine(idDomaine: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/cible/entreprise/number/' + idDomaine, {headers: httpHeaders}).map(
      number => number
    );
  }
  getAllTranches(): Observable<Taille[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/cible/AllTranche', {headers: httpHeaders});
  }


}
