import {Injectable, ɵConsole} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URLS} from '../../config/api.url.config';
import {SousDomaine} from '../domain/sous.domaine';
import {Domaine} from '../domain/domaine';

@Injectable()
export class SousDomaineService {

  constructor(private http: HttpClient) {
  }
  addSousDomaine(sousDomaine: SousDomaine): Observable<SousDomaine> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(API_URLS.SousDOMAINE_URLS, sousDomaine, {headers: httpHeaders}).map(
      domai => domai,
    );
  }
  deleteSousDomaine(sousDomaine: SousDomaine): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(API_URLS.SousDOMAINE_URLS + sousDomaine.idSousDomaine, {headers: httpHeaders, responseType: 'text'}).
    map(
      domai => domai,
    );
  }
  updateSousDomaine(sousDomaine: SousDomaine, domaine: Domaine): Observable<SousDomaine> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(API_URLS.SousDOMAINE_URLS + domaine.idDomaine + '/' + sousDomaine.idSousDomaine, sousDomaine,{headers: httpHeaders}).
    map(
      domai => domai,
    );
  }
  getConcerne(sousDomaine: SousDomaine): Observable<string> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.SousDOMAINE_URLS + 'number/sousDomaine/' + sousDomaine.idSousDomaine, {headers: httpHeaders, responseType: 'text'}) ;
  }
}
