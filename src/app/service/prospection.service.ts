import {Injectable} from '@angular/core';

import {Prospection} from '../domain/prospection';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URLS} from '../../config/api.url.config';
import {NgProgress} from 'ngx-progressbar';

@Injectable()
export class ProspectionService {

  constructor(private http: HttpClient) {

  }

  prospecterJumia(item: String): Observable<Prospection[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/prospection/' + item, {headers: httpHeaders});
  }

  prospecterPagesJaunes(item: String): Observable<Prospection[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/prospectionPageJaune/' + item, {headers: httpHeaders});
  }

  prospecterSunuEntreprises(item: String): Observable<Prospection[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + '/prospectionSunuEntreprise/' + item, {headers: httpHeaders});
  }
}
