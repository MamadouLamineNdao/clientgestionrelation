import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {PieceJointe} from '../domain/piece-jointe';
import {API_URLS} from "../../config/api.url.config";

@Injectable()
export class MediaService {
  baseURL = API_URLS.ADRESSE;
  constructor(private http: HttpClient) {
  }
  getMedias(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/medias', {headers: httpHeaders});

  }
  getMediaById(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/medias/' + id, {headers: httpHeaders});

  }
  createMedia(m: PieceJointe[]): Observable<PieceJointe[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/medias', m, {headers: httpHeaders});

  }
  editMedia(m: PieceJointe): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/medias/' + m.id, m, {headers: httpHeaders});

  }
  deleteMedia(m: PieceJointe): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/medias/' + m.id, {headers: httpHeaders, responseType: 'text'});

  }
}
