import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URLS} from '../../config/api.url.config';
import {Personne} from '../domain/personne';
import {Entreprise} from '../domain/entreprise';
import {Intermediaire} from '../domain/intermediaire';
import {Domaine} from '../domain/domaine';
import {Representant} from '../domain/representant';

@Injectable()
export class RepresentantService {

  constructor(private http: HttpClient) {

  }




  findRepresentantsByIntermediaire(idIntermediaire: number): Observable<Personne[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.CIBLE_URLS + idIntermediaire, {headers: httpHeaders});
  }




  addRepresentant(representant: Representant): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(API_URLS.CIBLE_URLS , representant, {headers: httpHeaders}).map(
      person => person,
    );
  }


  updateRepresentant(even: Representant): Observable<Personne> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(API_URLS.CIBLE_URLS + even.idRepresentant, even, {headers: httpHeaders});
  }


  deleteRepresentant(even: Representant): Observable<any> {
    console.log(even) ;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(API_URLS.CIBLE_URLS + even.idRepresentant, {headers: httpHeaders, responseType: 'text'}).map(
      personne => personne,
    )
      ;
  }



}
