import {Injectable, ɵConsole} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URLS} from '../../config/api.url.config';
import {Domaine} from '../domain/domaine';
import {Todo} from '../domain/todo';

@Injectable()
export class TodoService {

  constructor(private http: HttpClient) {
  }
  addTodo(todo: Todo): Observable<Todo> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(API_URLS.TodoCible, todo, {headers: httpHeaders}).map(
      data => data,
    );
  }
  deleteTodo(todo: Todo): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(API_URLS.TodoCible + todo.id, {headers: httpHeaders, responseType: 'text'}).
    map(
      data => data,
    );
  }
  updateTodo(todo: Todo): Observable<Todo> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(API_URLS.TodoCible + todo.id, todo, { headers: httpHeaders}).
    map(
      data => data,
    );
  }
  getListTodoCible(type: String): Observable<Todo[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(API_URLS.TodoCible + '' + type, {headers: httpHeaders}) ;
  }
}
