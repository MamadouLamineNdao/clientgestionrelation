
import {Injectable} from '@angular/core';
import {Evenement} from '../domain/evenement';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {map} from 'rxjs/operator/map';
import {Appel} from '../domain/appel';
import {Reunion} from '../domain/reunion';
import {Mail} from '../domain/mail';
import {Personne} from '../domain/personne';
import {Partenaire} from '../domain/partenaire';
import {inject} from '@angular/core/testing';
import {Intermediaire} from "../domain/intermediaire";
import {Tache} from "../domain/tache";
import {API_URLS} from "../../config/api.url.config";

@Injectable()
export class EvenementService {
  evenements: Evenement[] = [];
  appels: Appel[] = [];
  reunions: Reunion[] = [];
  mails: Reunion[] = [];
  baseURL = API_URLS.ADRESSE;

  constructor(private http: HttpClient) {
  }

  listeEvenement(): Observable<Evenement[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/evenements', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }
  intermediaireEvenement(id: number): Observable<Intermediaire> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + 'evenements/' + id, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  EvenementTypeAppel(id: number): Observable<Appel> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + 'evenement/appel/' + id, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }
  EvenementTypeReunion(id: number): Observable<Reunion> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + 'evenement/reunion/' + id, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }
  EvenementTypeMail(id: number): Observable<Intermediaire> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + 'evenement/mail/' + id, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }






  creerEvenement(even: Evenement): Observable<Evenement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/personnes', even, {headers: httpHeaders}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }
  creerAppel(id: number , even: Appel): Observable<Appel> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    even.dateCreation = new Date().getTime();

    return this.http.post(this.baseURL + id + '/evenement/appel', even, {headers: httpHeaders}).map(
      appel => appel,
    ).catch(error => Observable.throw(error));
  }

  creerMail(id: number , even: Mail): Observable<Mail> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    even.dateCreation = new Date().getTime();
    return this.http.post(this.baseURL + id + '/evenement/mail', even, {headers: httpHeaders}).map(
      mail => mail,
    ).catch(error => Observable.throw(error));
  }

  creerReunion(id: number , even: Reunion): Observable<Reunion> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    even.dateCreation = new Date().getTime();
    return this.http.post(this.baseURL + id + '/evenement/reunion', even, {headers: httpHeaders}).map(
      reunion => reunion,
    ).catch(error => Observable.throw(error));
  }

  updateEvenement(even: Evenement): Observable<Evenement> {
    const httpHeaders = new HttpHeaders();
    even.dateModification = new Date().getTime();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/personnes/' + even.idEvenement, even, {headers: httpHeaders}).map(
       event => event,
    ).catch(error => Observable.throw(error));
  }
  updateAppel(id: number, even: Appel): Observable<Appel> {
    const httpHeaders = new HttpHeaders();
    even.dateModification = new Date().getTime();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + id + '/evenement/appel/' + even.idEvenement, even, {headers: httpHeaders}).map(
       appel => appel,
    ).catch(error => Observable.throw(error));
  }

  updateMail(id: number, even: Mail): Observable<Mail> {
    const httpHeaders = new HttpHeaders();
    even.dateModification = new Date().getTime();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + id + '/evenement/mail/' + even.idEvenement, even, {headers: httpHeaders}).map(
       mail => mail,
    ).catch(error => Observable.throw(error));
  }

  updateReunion(id: number, even: Reunion): Observable<Reunion> {
    const httpHeaders = new HttpHeaders();
    even.dateModification = new Date().getTime();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + id + '/evenement/reunion/' + even.idEvenement, even, {headers: httpHeaders}).map(
       reunion => reunion,
    ).catch(error => Observable.throw(error));
  }

  delete(id: number, evenement: Evenement): Observable<any> {

    console.log(evenement.idEvenement);
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + 'intermediaires/' + id +  '/evenement/' +
      evenement.idEvenement, {headers: httpHeaders, responseType: 'text'}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }

  appelChart(id: string, year: number): Observable<Appel[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + 'intermediaire/' + id + '/evenement/appel/' + year, {headers: httpHeaders}).map(
      appel => appel,
    ).catch(error => Observable.throw(error));
  }
  mailChart(id: string , year: number): Observable<Mail[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + id + '/evenement/mail/' + year, {headers: httpHeaders}).map(
      mail => mail,
    ).catch(error => Observable.throw(error));
  }
  reunionChart(id: string , year: number): Observable<Reunion[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + id + '/evenement/reunion/' + year, {headers: httpHeaders}).map(
      reunion => reunion,
    ).catch(error => Observable.throw(error));
  }

  agendaSendTache(id: string , tache: Tache ): Observable<Tache> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + 'partenaires/' + id + '/intermediaire/tache', tache, {headers: httpHeaders}).map(
      tachel => tachel,
    ).catch(error => Observable.throw(error));
  }
  agendaUpdateTache(id: number , tache: Tache ): Observable<Tache> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + 'partenaires/' + id + '/intermediaire/tache/' + tache.idTache , tache, {headers: httpHeaders}).map(
      tachel => tachel,
    ).catch(error => Observable.throw(error));
  }
  listeTache(): Observable<Tache[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/partenaires/intermediaire/taches', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throw(error));
  }

  deleteTache(tache: Tache): Observable<any> {

    console.log(tache.idTache);
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + 'partenaires/intermediaire/tache/' + tache.idTache
      , {headers: httpHeaders, responseType: 'text'}).map(
      person => person,
    ).catch(error => Observable.throw(error));
  }



}
