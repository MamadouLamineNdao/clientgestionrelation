import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SelectModule} from 'ng2-select';
import {SharedFormCibleModule} from '../sharedFormCible/sharedFormModule';
import {PipeModule} from '../../shared/pipe/pipe.module';
import {DomaineComponent} from './domaine/domaine.component';
import {routing} from './parametre.routing';
import {ModalModule} from 'ngx-modal';
import {DomaineService} from '../../service/domaine.service';
import {SousDomaineService} from '../../service/sousDomaine';




@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    SharedFormCibleModule,
    PipeModule,
    ModalModule,
    routing,

  ],
  declarations: [
    DomaineComponent,
  ],

  exports: [
  ],
  providers: [ DomaineService, SousDomaineService ]
})
export class ParametreModule { }
