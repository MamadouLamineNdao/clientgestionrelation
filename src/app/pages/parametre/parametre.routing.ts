import {RouterModule, Routes} from '@angular/router';
import {DomaineComponent} from './domaine/domaine.component';

export const childRoutes: Routes = [

  {
    path: '',
    children: [
      { path: 'domaine', component: DomaineComponent},
    ]
  }
];

export const routing = RouterModule.forChild(childRoutes);
