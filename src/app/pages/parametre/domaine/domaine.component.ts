import { Component, OnInit } from '@angular/core';
import {DomaineService} from '../../../service/domaine.service';
import {Domaine} from '../../../domain/domaine';
import {SousDomaine} from '../../../domain/sous.domaine';
import swal from 'sweetalert2';
import {SousDomaineService} from '../../../service/sousDomaine';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-domaine',
  templateUrl: './domaine.component.html',
  styleUrls: ['./domaine.component.scss']
})
export class DomaineComponent implements OnInit {
  domaines: Domaine[];
  domaine: Domaine;
  sousDomaine: SousDomaine ;
  sousDomaines: SousDomaine [] ;
  modal;
  decor;
  headerModal;
  formDomaine: FormGroup;
  formSousDomaine: FormGroup;
  domaineTmp ;
  sousDomaineTmp ;

  constructor(private fb: FormBuilder, private sousDomaineService: SousDomaineService, private domaineService: DomaineService) {
    this.formDomaine = this.fb.group(
      {
        'nomDomaine': ['', Validators.required],

      }
    ) ;
    this.formSousDomaine = this.fb.group(
      {
        'libelle': ['', Validators.required],

      }
    ) ;
    // this.domaineTmp = JSON.parse(JSON.stringify(this.domaine));
    // this.sousDomaineTmp = JSON.parse(JSON.stringify(this.sousDomaine));
  }


  ngOnInit() {
    this.domaines = [] ;
    this.domaine = new Domaine();
    this.domaineTmp = new Domaine();
    this.sousDomaine = new SousDomaine() ;
    this.sousDomaineTmp = new SousDomaine() ;
    this.sousDomaines = [] ;
    this.domaineService.getAllDomaine().subscribe(
      data => {
        this.domaines = data;
        /*je l'ai remplacé par getConcerné*/

       for ( let i = 0 ; i < this.domaines.length; i++) {
          for (let j = 0 ; j < this.domaines[i].sousDomaines.length; j++) {
            this.sousDomaineService.getConcerne(this.domaines[i].sousDomaines[j]).subscribe(
              number => {

                this.domaines[i].sousDomaines[j].numberEntreprise = parseInt(number, 10) ;

              },
              error1 => {
                console.log(error1) ;
              }
            );
          }
        }
        if (this.domaines !== null && this.domaines.length !== 0) {
          this.changeDecor(this.domaines[0]);
        }
        console.log(this.domaines) ;
      },
      error => {
        console.log('An error was occured.');
      }
    ) ;

  }

  /* methode pour les modals */
  openModalDomaine(modal: any, domaine: Domaine, headerModal) {
    this.domaine = domaine;
    this.modal = modal;
    this.headerModal = headerModal ;
    this.domaineTmp = JSON.parse(JSON.stringify(this.domaine));
    modal.open();
  }
  openModalSousDomaine(modal: any, sousDomaine: SousDomaine) {
    this.sousDomaine = sousDomaine;
    this.sousDomaineTmp = JSON.parse(JSON.stringify(this.sousDomaine));
    this.modal = modal;
    modal.open();
  }

  openModal(modal) {
    this.modal = modal;
    modal.open();
  }

  closeModal() {
    this.modal.close();
  }

  addDomaine(modal: any) {
    this.domaine = new Domaine() ;
    this.changeDecor(this.domaine) ;
    this.openModalDomaine(modal, this.domaine, '') ;
    console.log(this.domaine.sousDomaines) ;
  }
  addSousDomaine(modal: any) {
    this.openModalSousDomaine(modal, new SousDomaine()) ;
  }


  changeDecor(domaine) {
    this.decor = domaine.nomDomaine;
    this.domaine = domaine ;
    this.sousDomaines = this.domaine.sousDomaines ;
    this.domaineTmp = JSON.parse(JSON.stringify(this.domaine));
    /*for ( let sousDomaine of this.sousDomaines) {
      this.getConcerne(sousDomaine) ;
    }*/

  }
  onSubmitDomaine(domaine: Domaine) {
    this.domaine = domaine ;
    if (domaine.idDomaine === null || domaine.idDomaine === undefined ) {
      this.domaineService.addDomaine(domaine).subscribe(
        data => {
          this.domaine = new Domaine() ;
          this.domaines.push(data) ;
          this.closeModal() ;
          data.sousDomaines = [] ;
          this.changeDecor(data);
        },
        error => { console.log('error') ; }
      ) ;
    } else {
      this.domaineService.updateDomaine(domaine).subscribe(
        data => {
          for (let i = 0; i < this.domaines.length; i++) {
            if (this.domaines[i].idDomaine === this.domaine.idDomaine) {
              this.domaines[i] = data ;
              break;
            }
          }
          this.closeModal() ;
          this.changeDecor(data);
        },
        error => {
          console.log('error uopdate');
        }
      ) ;
    }
  }
  onSubmitSousDomaine(sousDomaine: SousDomaine) {
    console.log('test1', sousDomaine) ;
    this.sousDomaine = sousDomaine ;

    if (sousDomaine.idSousDomaine === undefined ) {
      this.sousDomaine.domaine = this.domaine ;
      this.sousDomaineService.addSousDomaine(this.sousDomaine).subscribe(
        data => {
          console.log(this.sousDomaines) ;
          this.sousDomaines.push(data) ;
          this.closeModal() ;
        },
        error => { console.log('') ; }
      ) ;
    } else {
      this.domaine.sousDomaines = undefined;
      this.sousDomaine.domaine = this.domaine;
      this.sousDomaineService.updateSousDomaine(this.sousDomaine, this.domaine).subscribe(
        data => {
          console.log('update', data) ;
          this.closeModal();
        },
        error => {
          console.log('error update');
        }
      ) ;
      this.domaine.sousDomaines = this.sousDomaines ;
    }
  }

  onDeleteDomaine(domaine) {
    this.domaine = domaine ;
    swal({
      title: 'Etes vous sure?',
      text: 'Ce domaine sera définitivement supprimé!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimer',
      cancelButtonText: 'Non, Annulez'

    }).then((result) => {
      if (result.value) {
        this.domaineService.deleteDomaine(this.domaine).subscribe(
          () => {
            swal(
              'Deleted!',
              'Suppression reussie',
              'success'
            );
            for (let i = 0; i < this.domaines.length; i++) {
              if (this.domaines[i].idDomaine === this.domaine.idDomaine) {
                this.domaines.splice(i, 1);
                break;
              }
            }
            this.domaine = new Domaine()  ;
          },
          error =>
            swal(
              'Impossible',
              'Supprimer avant tous les sous-domaines',
              'error'
            )
        ) ;
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Annulée',
          'Supression du domaine annulée',
          'error'
        ) ;
      }
    });
  }
  onDeleteSousDomaine(sousDomaine) {
    this.sousDomaine = sousDomaine ;
    swal({
      title: 'Etes vous sur?',
      text:  'Ce sous-domaine sera définitivement supprimé',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimez ',
      cancelButtonText: 'Non, annuler'

    }).then((result) => {
      if (result.value) {
        this.sousDomaineService.deleteSousDomaine(sousDomaine).subscribe(
          () => {
            swal(
              'Deleted!',
              ' Suppression reussie.',
              'success'
            );
            for (let i = 0; i < this.sousDomaines.length; i++) {
              if (this.sousDomaines[i].idSousDomaine === this.sousDomaine.idSousDomaine) {
                this.sousDomaines.splice(i, 1);
                console.log('haaa') ;
                break;
              }
            }
            console.log(this.sousDomaines) ;

          },
          error =>
            swal(
              'Impossible',
              'Il y des organisations concernées',
              'error'
            )
        ) ;
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Annulée',
          'Suppression de sous-domaine annulée :)',
          'error'
        ) ;
      }
    });
  }
  getConcerne(sousDomaine: SousDomaine) {
    let resul = 0 ;
    console.log('getConcerne') ;
    this.sousDomaineService.getConcerne(sousDomaine).subscribe(
      number => {

        sousDomaine.numberEntreprise = parseInt(number, 10) ;
        console.log(resul) ;

      },
      error1 => {
        console.log(error1) ;
      }
    ) ;

  }
}
