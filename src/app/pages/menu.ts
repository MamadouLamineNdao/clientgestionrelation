export let MENU_ITEM = [
  /*{
    path: 'accueil',
    title: 'Accueil2',
    icon: 'dashboard'
  },*/
  {
    path: 'prospection',
    title: 'Prospection',
    icon: 'cogs'
  },
/*  {
    path: 'index',
    title: 'Accueil',
    icon: 'dashboard'
  },*/
  {
    path: 'cible',
    title: 'Cible',
    icon: 'user',
    children: [
      {
        path: 'personne',
        title: 'Professionnels',
      },
      {
        path: 'entreprise',
        title: 'Organisations',
      },
    ]
  },

  {
    path: 'partenaire/intermediaire',
    title: 'Partenaire',
    icon: 'table',

  },
  {
    path: 'partenaire/officiel',
    title: 'officiel',
    icon: 'check-square-o',

  },
  {
    path: 'parametre',
    title: 'Parametre',
    icon: 'cog',
    children: [
      {
        path: 'domaine',
        title: ' Fixer les domaines ',
      },

    ]
  },

  /* {
       path: 'editor',
       title: 'Pell Editor',
       icon: 'pencil'
   },
   {
       path: 'icon',
       title: 'Icon',
       icon: 'diamond'
   },
   {
       path: 'profile',
       title: 'User Profile',
       icon: 'user'
   },
   {
       path: 'ui',
       title: 'UI Element',
       icon: 'paint-brush',
       children: [
           {
               path: 'grid',
               title: 'Bootstrap Grid'
           },
           {
               path: 'buttons',
               title: 'Buttons'
           },
           {
               path: 'notification',
               title: 'Notification'
           },
           {
               path: 'tabs',
               title: 'Tabs'
           },
           {
               path: 'file-tree',
               title: 'File Tree'
           },
           {
               path: 'modals',
               title: 'Modals'
           },
           {
               path: 'progress-bar',
               title: 'ProgressBar'
           },
           {
                path: 'loading',
                title: 'Loading'
            },
       ]
   },
   {
       path: 'form',
       title: 'Forms',
       icon: 'check-square-o',
       children: [
           {
               path: 'form-inputs',
               title: 'Form Inputs'
           },
           {
               path: 'form-appel',
               title: 'Form Layouts'
           },
           {
               path: 'file-upload',
               title: 'File Upload'
           },
           {
               path: 'ng2-select',
               title: 'Ng2-Select'
           }
       ]
   },
   {
       path: 'charts',
       title: 'Charts',
       icon: 'bar-chart',
       children: [
           {
               path: 'echarts',
               title: 'Echarts'
           }
       ]
   },
   {
       path: 'table',
       title: 'Tables',
       icon: 'table',
       children: [
           {
               path: 'basic-tables',
               title: 'Basic Tables'
           },
           {
               path: 'data-table',
               title: 'Data Table'
           }
       ]
   },
   {
       path: 'menu-levels',
       title: 'Menu Levels',
       icon: 'sitemap',
       children: [
           {
               path: 'levels1',
               title: 'Menu Level1',
               children: [
                   {
                       path: 'levels1-1',
                       title: 'Menu Level1-1'
                   }
               ]
           },
           {
               path: 'levels2',
               title: 'Menu Level2'
           }
       ]
   },*/
];
