import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './profile.routing';
import { SharedModule } from '../../shared/shared.module';
import { ProfileComponent } from './profile.component';
import {LayoutModule} from '../../shared/layout.module';
import {PagesComponent} from '../pages.component';

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        routing
    ],
    declarations: [
        ProfileComponent,
        PagesComponent,
    ]
})
export class ProfileModule { }
