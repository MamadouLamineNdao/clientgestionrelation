import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Domaine} from '../../../domain/domaine';
import {DomaineService} from '../../../service/domaine.service';
import {Modal} from 'ngx-modal';

@Component({
  selector: 'app-domaineForm',
  templateUrl: './domaineForm.component.html',
  styleUrls: ['../form.component.scss']
})
export class DomaineFormComponent implements  OnInit, OnChanges {
  @Input() domaine: Domaine ;
  domaineTmp: Domaine ;

  formDomaine: FormGroup;
  @Output() back= new EventEmitter() ;
  constructor(private fb: FormBuilder, private domaineService: DomaineService) {
    this.formDomaine = this.fb.group(
      {
        'nomDomaine': ['', Validators.required],

      }
    ) ;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.domaine = new Domaine() ;
    this.domaineTmp = JSON.parse(JSON.stringify(this.domaine));
  }
  ngOnInit(): void {
    this.domaine = new Domaine() ;

  }

  onSubmitDomaine(domaine: Domaine) {
    this.domaine = domaine ;
    if (this.domaine.idDomaine === null || this.domaine.idDomaine === undefined ) {
      this.domaineService.addDomaine(domaine).subscribe(
        data => {
          data.sousDomaines = [] ;
          this.back.emit(data) ;
          this.domaine = new Domaine();
        },
        error => { console.log('error') ; }
      ) ;
    } else {
      this.domaineService.updateDomaine(domaine).subscribe(
        data => {
        },
        error => {
         this.back.emit() ;
          console.log('error uopdate');
        }
      ) ;
    }
  }





}
