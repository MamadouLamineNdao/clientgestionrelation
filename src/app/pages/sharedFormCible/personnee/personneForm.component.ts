  import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Personne} from '../../../domain/personne';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CibleService} from '../../../service/cible.service';

@Component({
  selector: 'app-personneForm',
  templateUrl: './personneForm.component.html',
  styleUrls: ['../form.component.scss']

})
export class PersonneFormComponent implements OnInit, OnChanges {
  @Input() personne: Personne;
  @Input()personnes: Personne[];
  @Output() back: EventEmitter<Personne> = new EventEmitter() ;
  personneTmp: Personne ;

  formPerson: FormGroup ;

  constructor(private fb: FormBuilder, private cibleService: CibleService) {
    this.formPerson = this.fb.group(
      {
        'nom': ['', Validators.required],
        'adresseMail': [''],
        'profession': ['' , Validators.required],
        'adresse': ['' ],
        'telephone': [''],
        'codePostal': [''],
        'prenom': ['' ]
      }
    ) ;
    this.personnes = [] ;
    this.personneTmp = new Personne() ;
  }
  ngOnInit(): void {
     // this.personneTmp = JSON.parse(JSON.stringify(this.personne));
  }
  ngOnChanges(): void {
    if(this.personne)
      this.personneTmp = JSON.parse(JSON.stringify(this.personne));

}
  onSubmit() {
    if ( this.personne.idCible === undefined) {
      this.cibleService.addPersonnes(this.personneTmp).subscribe(
        pers => {
          this.personnes.push(pers) ;
          this.back.emit(pers);
        },
        error => {
          console.log(error);
        }
      );

    }else {
      this.cibleService.updatePersonne(this.personneTmp).subscribe(
        pers => {
          for (let i = 0; i < this.personnes.length; i++) {
            if (this.personnes[i].idCible === pers.idCible) {
              this.personnes[i] = pers ;
            }
          }
          this.back.emit(pers);
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
