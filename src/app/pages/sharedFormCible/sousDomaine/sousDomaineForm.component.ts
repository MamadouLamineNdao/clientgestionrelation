import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Domaine} from '../../../domain/domaine';
import {SousDomaine} from '../../../domain/sous.domaine';
import {SousDomaineService} from '../../../service/sousDomaine';
@Component({
  selector: 'app-sousDomaineForm',
  templateUrl: './sousDomaineForm.component.html',
  styleUrls: ['../form.component.scss']
})
export class SousDomaineFormComponent implements  OnInit, OnChanges {
  @Input()domaine: Domaine ;
  sousDomaine: SousDomaine ;
  formSousDomaine: FormGroup;
  @Output() back= new EventEmitter() ;
  constructor(private fb: FormBuilder, private sousDomaineService: SousDomaineService) {
    this.formSousDomaine = this.fb.group(
      {
        'libelle': ['', Validators.required],
      }
    ) ;
  }
  

  

  ngOnChanges(changes: SimpleChanges): void {


  }
  ngOnInit(): void {
    this.sousDomaine = new SousDomaine() ;


  }
  onSubmitSousDomaine(sousDomaine: SousDomaine) {
    this.sousDomaine = sousDomaine ;
    if (sousDomaine.idSousDomaine === undefined ) {
      /* copier pour ne pas le perdre */
      let sousDomainesCopie = JSON.parse(JSON.stringify(this.domaine.sousDomaines));

      /*pour eviter une boucle infinie avec la transformation en Json*/
      this.domaine.sousDomaines = undefined ;

      this.sousDomaine.domaine = this.domaine ;
      this.sousDomaineService.addSousDomaine(this.sousDomaine).subscribe(
        data => {
          console.log('create sous-domaine') ;
          sousDomainesCopie.push(data) ;
          this.domaine.sousDomaines = sousDomainesCopie ;
          this.back.emit(data)  ;
          this.sousDomaine = new SousDomaine();
        },
        error => {
          console.log('') ;
        }
      ) ;
    }

  }


}

