import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Representant} from '../../../domain/representant';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Entreprise} from '../../../domain/entreprise';
import swal from 'sweetalert2';
import {RepresentantService} from '../../../service/representant.service';
import {Intermediaire} from "../../../domain/intermediaire";

@Component({
  selector: 'app-representantForm',
  templateUrl: './representantForm.component.html',
  styleUrls: ['../form.component.scss']

})
export class RepresentantFormComponent implements OnInit, OnChanges {
  @Output()back: EventEmitter<Representant> = new EventEmitter();
  @Input() representant: Representant;
  @Input() entreprise: Entreprise ;
  representantTmp: Representant ;

  id: number;
  message: string;

  formRepresentant: FormGroup ;

  constructor(private fb: FormBuilder, private representantService: RepresentantService) {
    this.formRepresentant = this.fb.group(
      {
        'nom': ['', Validators.required],
        'prenom': [''],
        'email': [''],
        'tel': [''],
        'fonction': ['']

      }
    );
  }

  ngOnInit() {

  }
  ngOnChanges(): void {
    // this.representant = new Representant() ;
    // this.entreprise = new Entreprise() ;
    if ( this.representant !== undefined) {
      this.representantTmp = JSON.parse(JSON.stringify(this.representant));
    }
  }





  onSubmit() {
  if (this.representant.idRepresentant === undefined) {
    this.representantService.creerRepresentant(this.entreprise.idCible, this.representantTmp).subscribe(
      representant => {
        this.back.emit(representant);
        this.entreprise.representants.push(representant)
        representant = new Representant();
        this.formRepresentant.reset();
        swal('Succès', 'le représentant a été créé', 'success');

      },
      error => {
        console.log(error);
        swal('Erreur', 'Il y a eu un probléme. Veuillez reassez svp !', 'error');
      }
    );
  } else {
    this.representantService.updateRepresentant(this.entreprise.idCible, this.representantTmp).subscribe(
      representant => {
        this.back.emit(representant);
        for (let i = 0; i < this.entreprise.representants.length; i++) {
          if (this.entreprise.representants[i].idRepresentant === this.representant.idRepresentant) {
            this.entreprise.representants[i] = representant;
            break;
          }
        }
        swal('Succès', 'le représentant a été mis à jour', 'success');

      },
      error => {
        console.log(error);
        swal('Erreur', 'Il y a eu un probléme. Veuillez reassez svp !', 'error');

      }
    );
  }


}
 }
