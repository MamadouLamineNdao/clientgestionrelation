import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {NgxEchartsModule} from 'ngx-echarts';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RepresentantFormComponent} from './representant/representantForm.component';
import {EntrepriseFormComponent} from './entreprise/entrepriseForm.component';
import {PersonneFormComponent} from './personnee/personneForm.component';
import {CibleService} from '../../service/cible.service';
import {MultipleSelectComponent} from './multiple-select/multiple-select.component';
import {SelectModule} from 'ng2-select';
import {DomaineService } from '../../service/domaine.service';
import {ModalModule} from 'ngx-modal';
import {ParametreModule} from '../parametre/parametre.module';
import {DomaineFormComponent} from './domaine/domaineForm.component';
import {SousDomaineFormComponent} from './sousDomaine/sousDomaineForm.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgxEchartsModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    ModalModule,

  ],
  declarations: [
    PersonneFormComponent,
    EntrepriseFormComponent,
    RepresentantFormComponent,
    MultipleSelectComponent,

    DomaineFormComponent,

    SousDomaineFormComponent


  ],
  exports: [
    RepresentantFormComponent,
    EntrepriseFormComponent,
    PersonneFormComponent,
  ],
  providers: [CibleService, DomaineService ]
})
export class SharedFormCibleModule { }


