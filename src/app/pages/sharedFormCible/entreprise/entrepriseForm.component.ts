import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Entreprise} from '../../../domain/entreprise';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Domaine} from '../../../domain/domaine';
import {DomaineService} from '../../../service/domaine.service';
import {Representant} from '../../../domain/representant';
import {SelectItem} from 'ng2-select';
import {SousDomaine} from '../../../domain/sous.domaine';
import {CibleService} from '../../../service/cible.service';
import {Personne} from '../../../domain/personne';
import {Taille} from '../../../domain/taille';
import {Partenaire} from '../../../domain/partenaire';
@Component({
  selector: 'app-entrepriseForm',
  templateUrl: './entrepriseForm.component.html',
  styleUrls: ['../form.component.scss']
})
export class EntrepriseFormComponent implements  OnInit, OnChanges {
  @Input() entreprise: Entreprise ;
  @Input() entreprises: Entreprise[] ;
  domaines: Domaine[] ;
  domaine: Domaine ;
  @Output() back: EventEmitter<Entreprise> = new EventEmitter() ;
  @Output() back2 = new EventEmitter() ;
  public entrepriseTmp  ;
  formEntreprise: FormGroup ;
  @Input() value: Array<any> = [];
  public disabled = false;
  public items: Array<any>;
  @ViewChild('domaineForm') domaineForm ;
  @ViewChild('sousDomaineForm') sousDomaineForm ;
  allTailles: Taille[] ;
  reload = true;

  constructor(private fb: FormBuilder, private domaineService: DomaineService, private cibleService: CibleService) {
    this.formEntreprise = this.fb.group(
      {
        'nomEntreprise': ['', Validators.required],
        'adresse': [''],
        'adresseMail': [''],
        'telephone': [''],
        'codePostal': [''],
        'tailleEntreprise': ['', Validators.required],
        'siteInternet': [''],
        'domaine': [''],
      }
    ) ;
    this.entrepriseTmp = new Entreprise() ;

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.entreprise)
      this.entrepriseTmp = JSON.parse(JSON.stringify(this.entreprise));
  }
  ngOnInit(): void {
    this.entreprises = [];
    this.chargerAllDomaine() ;
    this.getAllTaille() ;


  }
  chargerAllDomaine() {
    this.domaineService.getAllDomaine().subscribe(
      data => {
        this.domaines = data ;
      }
    ) ;
  }
  getAllTaille() {
    this.cibleService.getAllTranches().subscribe(
      data => {
        this.allTailles = data ;
      }
    ) ;
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  removed($event) {
    console.log($event);
    console.log(this.value) ;
  }
  selected($event) {
    console.log($event);
    console.log(this.value) ;

  }

  setItems() {
    this.reload = false;
    setTimeout(() => {
      this.reload = true;
    }, 1000);
    if (this.domaine !== null && this.domaine !== undefined
      && this.domaine.sousDomaines !== null && this.domaine.sousDomaines !== undefined) {
      this.items = [];
      for (const sousDomaine of this.domaine.sousDomaines) {
        const item = {
          id: sousDomaine.idSousDomaine,
          text: sousDomaine.libelle
        };
        this.items.push(item);
      }
      console.log(this.items)
    }

  }
  addSousDomaine() {
    this.sousDomaineForm.open() ;
  }
  compareTaille(t1: Taille, t2: Taille): boolean {
    return (t1 && t2) && (t1.max === t2.max) && (t1.min === t2.min) ;
  }
  compare( dom1: Domaine , dom2: Domaine): boolean {
    return (dom1 && dom2) && (dom1.idDomaine === dom2.idDomaine);
  }
  onSubmit() {
    let sousDomainesTmp: any[] = [] ;
    for (const sousDomaine of this.value) {
      const item = {
        idSousDomaine: sousDomaine.id
      };
      sousDomainesTmp.push(item);
    }
    this.entrepriseTmp.sousDomaines = sousDomainesTmp ;
    if (this.entreprise.idCible === null || this.entreprise.idCible === undefined) {
      this.cibleService.addEntreprise(this.entrepriseTmp).subscribe(
        entrep => {
          this.back.emit(entrep);
          this.back2.emit(this.domaine);
          this.formEntreprise.reset()
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.cibleService.updateEntreprise(this.entrepriseTmp).subscribe(
        entrep => {
          for (let i = 0; i < this.entreprises.length; i++) {
            if (this.entreprises[i].idCible === entrep.idCible) {
              this.entreprises[i] = entrep ;
            }
          }
          this.back.emit(entrep);
          if (this.domaine !== null && this.domaine !== undefined)
            this.back2.emit(this.domaine);
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  openModal(modal: any) {
    modal.open() ;
  }

  doAddDomaine() {
    this.domaineForm.open();
  }

  addValue(event: SousDomaine) {
    let item = {
      id: event.idSousDomaine,
      text: event.libelle
    };
    this.value.push(item) ;
    this.value = JSON.parse(JSON.stringify(this.value));
    this.setItems();
  }

  fixerDomaine(domaine: Domaine) {
    this.domaine = domaine ;
    this.domaines.push(this.domaine);
    this.setItems();
  }
}


