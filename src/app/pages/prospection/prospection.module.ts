import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './prospection.routing';
import { SharedModule } from '../../shared/shared.module';
import {ProspectionComponent} from './prospection.component';
import {ListeProspectionComponent} from './components/listeProspection/listeProspection.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxEchartsModule} from 'ngx-echarts';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SelectModule} from 'ng2-select';
import {ModalModule} from 'ngx-modal';
import {SharedFormCibleModule} from '../sharedFormCible/sharedFormModule';
import {OrderModule} from 'ngx-order-pipe';
import {PipeModule} from '../../shared/pipe/pipe.module';
import {ProfileSiteComponent} from './components/profile/profile.site.component';
import {NgProgressModule} from 'ngx-progressbar';
import {NgxLoadingModule} from 'ngx-loading';
import {DomaineComponent} from '../parametre/domaine/domaine.component';
import {PagesComponent} from '../pages.component';
import {LayoutModule} from '../../shared/layout.module';


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxPaginationModule,
        NgxEchartsModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule,
        ModalModule,
        LayoutModule,
        SharedFormCibleModule,
        OrderModule,
        PipeModule,
        NgProgressModule,
        NgxLoadingModule,
        routing,


    ],
    declarations: [
        ProspectionComponent,
        ListeProspectionComponent,
        ProfileSiteComponent,
    ]
})
export class ProspectionModule { }
