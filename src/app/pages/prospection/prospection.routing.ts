import { Routes, RouterModule } from '@angular/router';
import {ProspectionComponent} from './prospection.component';

const childRoutes: Routes = [
    {
        path: '',
        component: ProspectionComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
