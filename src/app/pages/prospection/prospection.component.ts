import { Component, OnInit } from '@angular/core';
import {Prospection} from '../../domain/prospection';
import {GlobalService} from '../../shared/services/global.service';
import {RootComponent} from '../../shared/roots/root.component';
import {ProspectionService} from '../../service/prospection.service';

@Component({

  selector: 'app-prospection',
  templateUrl: './prospection.component.html',
  providers: [ProspectionService]

})
export class ProspectionComponent implements OnInit {

  prospections: Prospection[];

  constructor() {

  }

  ngOnInit() { }


  loadProspect(event) {
    this.prospections = event;
  }

}
