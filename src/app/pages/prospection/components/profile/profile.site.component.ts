import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProspectionService} from '../../../../service/prospection.service';
import {Prospection} from '../../../../domain/prospection';
import {ProspectionStorageService} from '../../../../service/prospection.storage.service';

@Component({
  selector: 'app-profile-site',
  templateUrl: './profile.site.component.html',
  styleUrls: ['./profile.site.component.scss'],
})
export class ProfileSiteComponent implements OnInit {
  avatarImgSrc = 'assets/images/nao.jpg';
  userName = 'PESI';
  userPost = 'Détecteur de potentiel partenaire';
  @Output() back: EventEmitter<Prospection[]> = new EventEmitter();
  @Output() show: EventEmitter<boolean> = new EventEmitter();
  choix: String;

  constructor(private prospectionService: ProspectionService, private prospectStorage: ProspectionStorageService) { }

  ngOnInit() {
  }

  prospectonsJumia() {
    this.prospectStorage.loading = true;
    this.show.emit(this.prospectStorage.loading);
    console.log(this.choix) ;
    this.prospectionService.prospecterJumia(this.choix).subscribe(entreps => {
        this.prospectStorage.prospects = entreps;
        this.back.emit(entreps);
      },
      error1 => {
        this.show.emit(false);
        this.prospectStorage.prospects = undefined;
      });
    console.log(this.show);
  }

  onSelect(val){
    console.log(val);
    this.choix = val;
  }

  prospectonsPagesJaunes() {
    this.prospectStorage.loading = true;
    this.show.emit(this.prospectStorage.loading);
    console.log(this.choix) ;
    this.prospectionService.prospecterPagesJaunes(this.choix).subscribe(entreps => {
        this.prospectStorage.prospects = entreps;
        this.back.emit(entreps);
      },
      error1 => {
        this.show.emit(false);
        this.prospectStorage.prospects = undefined;
      });
    console.log(this.show);
  }

  prospectonsSunuEntreprises() {
    this.prospectStorage.loading = true;
    this.show.emit(this.prospectStorage.loading);
    console.log(this.choix) ;
    this.prospectionService.prospecterSunuEntreprises(this.choix).subscribe(entreps => {
        this.prospectStorage.prospects = entreps;
        this.back.emit(entreps);
      },
      error1 => {
        this.show.emit(false);
        this.prospectStorage.prospects = undefined;
      });
    console.log(this.show);
  }

}
