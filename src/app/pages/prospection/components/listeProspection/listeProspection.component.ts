import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Prospection} from '../../../../domain/prospection';
import {ProspectionStorageService} from '../../../../service/prospection.storage.service';
import {Entreprise} from '../../../../domain/entreprise';
import swal from "sweetalert2";


@Component({
  selector: 'app-listeprospection',
  templateUrl: './listeProspection.component.html',
  styleUrls: ['./listeProspection.component.scss'],

})

export class ListeProspectionComponent implements OnInit, OnChanges {
  @Input() prospections: Prospection[];
  @Input() loading: boolean;

  decor = 'tic' ;
  modal: any ;
  order ;
  reverse;
  sousDomainesCheck: number[] = [] ;
  filtercolumn: string[] ;
  filter: string ;
  aProposSelected: string;
  titleSelected: string;

  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  /*number*/
  number = {} ;
  defaultContent= '';
  entreprise: Entreprise = new Entreprise();
  nomSelected: string;

  constructor(public prospectStorage: ProspectionStorageService) {
    this.sousDomainesCheck = [] ;
  }

  ngOnInit() {


  }


  /*test*/
  clickFilter(sd, event) {
    console.log(event.currentTarget.checked);
    if ( event.currentTarget.checked) {
      this.sousDomainesCheck.push(sd.idSousDomaine) ;
    }else {
      for (let i = 0; i < this.sousDomainesCheck.length; i++) {
        if (this.sousDomainesCheck[i] === sd.idSousDomaine) {
          this.sousDomainesCheck.splice(i, 1);
          break;
        }
      }
    }
    console.log(this.sousDomainesCheck) ;
    this.chargerEntreprise() ;

  }
  /*charger les entreprises séléectionnés*/

  chargerEntreprise() {
    /*this.cibleService.showEntreprises(this.TabNumberToString(this.sousDomainesCheck)).subscribe(
      entreps => {
        this.entreprises = entreps;
      },
      error1 => {console.log('An error was occured. getAlldomaine');
      }

    ) ;*/
  }

  /*transformer le tableau de id n string pour l'envoyer par l'url*/
  TabNumberToString(tabNumber: number[]): string {
    let resultTab: string[] = [] ;
    for ( let id of tabNumber){
      resultTab.push(id.toString()) ;
    }
    return resultTab.join(',') ;

  }


  /*Pagination du tableau*/
  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }


  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }
  filterDefineColumn() {
    if (this.filter === null || this.filter === undefined || this.filter === '' ) {
      this.filtercolumn = [] ;
    }else {
      this.filtercolumn = [this.filter, 'nomEntreprise', 'dateValidationSuggestion', 'domaine'] ;
    }
  }

  openModalCible(modal: any, prospection: Prospection) {
    this.entreprise = new Entreprise();
    this.nomSelected = prospection.nomEntreprise;
    this.entreprise.nomEntreprise = prospection.nomEntreprise;
    if(prospection.siteWeb){
      const tab = prospection.siteWeb.split('"');
      if(tab.length === 1){
        this.entreprise.siteInternet = prospection.siteWeb;
      }
      if (tab.length > 1){
        this.entreprise.siteInternet = tab[1];
      }
    }

    this.entreprise.adresse = prospection.adresse;
    console.log(this.entreprise.nomEntreprise);
    this.modal = modal;
    this.modal.open();
  }

  openModal(modal, apropos, entreprise) {
    this.aProposSelected = apropos;
    this.titleSelected = entreprise;
    modal.open();
  }

  closeModal(modal) {
    modal.close();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.prospections === undefined || this.prospections === null){
      this.prospections = this.prospectStorage.prospects;
      this.loading = this.prospectStorage.loading;
    }
  }

  closeForm() {
    this.modal.close() ;
    swal('Succès', 'Organisation ciblée', 'success');

  }
}
