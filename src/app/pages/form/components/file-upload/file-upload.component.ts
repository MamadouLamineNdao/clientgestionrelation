import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {


  constructor() { }

  ngOnInit() { }

  public uploader: FileUploader = new FileUploader({ url: URL});

  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
    this.getBase64();

  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }
   getBase64() {
    let file: any;
    for (const item of this.uploader.queue){
      file = item.file;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        console.log(reader.result);
      };
      reader.onerror = function (error) {
        console.log('Error: ', error);
      };
    }

  }
}
