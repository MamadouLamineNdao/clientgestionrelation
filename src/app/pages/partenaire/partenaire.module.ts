import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './partenaire.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxEchartsModule } from 'ngx-echarts';
import {NgxPaginationModule, PaginationControlsDirective} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PartenaireDetailComponent} from './detail-partenaire/partenaire-detail.component';
import {PartenaireEntrepriseComponent} from './partenaire-entrprise/partenaire-entreprise.component';
import {PartenairePersonneComponent} from './partenaire-personne/partenaire-personne.component';
import {ModalsComponent} from './detail-partenaire/modal-info/modals.component';
import {ModalModule} from 'ngx-modal';
import {FormAppelComponent} from './evenement/form-appel/form-appel.component';
import {RatingModule} from 'ng2-rating';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {FormMailComponent} from './evenement/form-mail/form-mail.component';
import {FormReunionComponent} from './evenement/form-reunion/form-reunion.component';
import {VerticalTimelineModule} from 'angular-vertical-timeline';
import {SharedFormCibleModule} from '../sharedFormCible/sharedFormModule';
import {DetailComponent} from './evenement/detail.component';
import {FileUploadModule} from 'ng2-file-upload';
import {FileUploadComponent} from '../form/components/file-upload/file-upload.component';
import {ChartsModule} from 'ng2-charts';
import {AgendaComponent} from './evenement/evenement-venir/agenda.component';
import {TacheComponent} from './detail-partenaire/todolist/tache.component';
import {PipeModule} from "../../shared/pipe/pipe.module";
import {OrderModule} from "ngx-order-pipe";
import {PartenaireOffiielComponent} from './partenaire-officiel/partenaire-offiiel.component';
import {FormOfficielComponent} from './evenement/form-offiiel/form-officiel.component';


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxEchartsModule,
        routing,
        NgxPaginationModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        DateTimePickerModule,
        VerticalTimelineModule,
        SharedFormCibleModule,
        FileUploadModule,
        ChartsModule,
        PipeModule,
        OrderModule








    ],
    declarations: [
      PartenaireDetailComponent,
      PartenaireEntrepriseComponent,
      PartenairePersonneComponent,
      ModalsComponent,
      DetailComponent,
      FormAppelComponent,
      FormMailComponent,
      FormReunionComponent,
      FormOfficielComponent,
      FileUploadComponent,
      AgendaComponent,
      TacheComponent,
      PartenaireOffiielComponent




    ]
})
export class PartenaireModule { }
