
import {PartenaireService} from '../../../service/partenaire.service';
import {Component, OnInit} from '@angular/core';
import {Partenaire} from '../../../domain/partenaire';
import {Intermediaire} from '../../../domain/intermediaire';
import {Personne} from '../../../domain/personne';
import {Entreprise} from '../../../domain/entreprise';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-personne',
  templateUrl: './partenaire-personne.component.html',
  styleUrls: ['./partenaire-personne.component.scss'],
  providers: [PartenaireService]
})
export class PartenairePersonneComponent implements OnInit {

  partenaires: Partenaire[] = [];
  intermediairesPer: Intermediaire[] = [];
  personnes: Personne[] = [];
  entreprises: Entreprise[] = [];
  entrepriseDetail: Entreprise;
  personneDetail: Personne;
  id: string;


  tableData: Array<any>;

  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;

  constructor(private partenaireService: PartenaireService , private router: Router , private route : ActivatedRoute) { }

  ngOnInit() {
    this.partenaireService.listeIntermediairePersonne().subscribe(
      data => {this.intermediairesPer = data;
        console.log(this.intermediairesPer);
      },
      error => {console.log('An error was occured.'); }
    );

  }
  onDetail(id: number, type: string): void {
    this.router.navigate(['./detail', id, type], {relativeTo: this.route });
  }

  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }

}
