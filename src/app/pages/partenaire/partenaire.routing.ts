import { Routes, RouterModule } from '@angular/router';
import {PartenaireDetailComponent} from './detail-partenaire/partenaire-detail.component';
import {PartenaireEntrepriseComponent} from './partenaire-entrprise/partenaire-entreprise.component';
import {PartenairePersonneComponent} from './partenaire-personne/partenaire-personne.component';
import {DetailComponent} from './evenement/detail.component';
import {PartenaireOffiielComponent} from './partenaire-officiel/partenaire-offiiel.component';

const childRoutes: Routes = [
          {
            path: 'intermediaire',
            component: PartenaireEntrepriseComponent,

          }
          ,
          {
            path: 'intermediaire/detail/:id/:type',
            component: PartenaireDetailComponent
          },
          {
            path: 'intermediaire/detail/:id/:type/appel',
            component: DetailComponent

          },
          {
            path: 'intermediaire/detail/:id/mail',
            component: DetailComponent

          },
          {
            path: 'intermediaire/detail/:id/reunion',
            component: DetailComponent

          },
          {
            path: 'intermediaire/detail/:id/appel',
            component: DetailComponent

          },
          {
            path: 'intermediaire/detail/:id/mail',
            component: DetailComponent

          },
          {
            path: 'intermediaire/detail/:id/reunion',
            component: DetailComponent

          },
          {
            path: 'officiel',
            component: PartenaireOffiielComponent

          }

        ]
;

export const routing = RouterModule.forChild(childRoutes);
