import {
  AfterContentChecked,
  AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output,
  SimpleChanges, ViewChild
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Appel} from '../../../../domain/appel';
import {EvenementService} from '../../../../service/evenement.service';
import {Representant} from '../../../../domain/representant';
import pell from 'pell';
import swal from 'sweetalert2';
import {PieceJointe} from "../../../../domain/piece-jointe";
import {noUndefined} from "@angular/compiler/src/util";
import {Entreprise} from '../../../../domain/entreprise';
import {fr} from '../../../../shared/global/fr';


@Component({
  selector: 'app-form-appel',
  templateUrl: './form-appel.component.html',
  styleUrls: ['./form-appel.component.scss']
})
export class FormAppelComponent implements OnInit, AfterViewChecked, OnChanges {

  formAppel: FormGroup;
  @Output()back: EventEmitter<Appel> = new EventEmitter();
  @Input()appel: Appel;
  @Input()representants: Representant[];
  @Input() entreprise: Entreprise;
  @Input()isReady = false;
  @Input() id: number;
  message: string;
  @Input() defaultContent = '';
  @Input() isChangeA = false;
  $this: any;
  @Input() appels: Appel[];
  @Input() type: string;
  isEntreprise = false;
  isProfessionel = false;
  appelTmp: Appel;
  obj = '';
  fr = fr;
  stateClass: string;
  representant: Representant ;
  public resume: string;

  /*pour gérer l'ouverture du modal depuis le component */
  @ViewChild('representantForm') modal;

  constructor(private fB: FormBuilder , private evenementService: EvenementService) {
    this.appel = new Appel();
    this.formAppel = this.fB.group(
      {
        'objectif': [''],
        'lieuEvenement': [''],
        'nom': ['', Validators.required],
        'numero': ['', Validators.required],
        'resume': [''],
        'dateEvenement': ['', Validators.required],
        'representant': [''],
        'appelEtat': ['']


      });
  }
  ngOnInit() {
    this.representant = new Representant() ;
    if (this.type === 'entreprise') {
      this.isEntreprise = true;
    }
    if (this.type === 'personne') {
      this.isProfessionel = true;
    }

  }
  ngOnChanges(): void {
    if ( this.appel !== null || this.appel !== undefined) {
      this.appelTmp = JSON.parse(JSON.stringify(this.appel));
      this.stateClass = this.appelTmp.etat === true ? 'fa fa-toggle-on' : 'fa fa-toggle-off';

    }
  }


  addObejctif() {
    if (this.obj === '' || this.obj === undefined) { return ; }
    this.appelTmp.objectif.push(this.obj);

    this.obj = '';

  }
  deleteObj( obj) {
    for (let i = 0; i < this.appelTmp.objectif.length ; i++) {
      if (this.appelTmp.objectif[i] === obj) {
        this.appelTmp.objectif.splice(i, 1);
        break;
      }
    }

  }



  onContentChange(event: string) {
    console.log(event);
  }

  onSubmit() {
    if (this.obj !== null && this.obj !== undefined && this.obj !== '' ){
      this.addObejctif();
    }
    if (this.appel.idEvenement === undefined) {
      this.evenementService.creerAppel(this.id , this.appelTmp).subscribe(
        appel => {
          this.back.emit(appel);
          this.appels.push(appel);
          this.message = 'Successfully created!';
          this.formAppel.reset();
          this.obj = '';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          swal({
            type: 'success',
            title: 'appel enregistré avec succes!',
            text: ' fermer'
          });

        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.evenementService.updateAppel(this.id , this.appelTmp).subscribe(
        appel => {
          this.back.emit(appel);
          for (let i = 0; i < this.appels.length; i++) {
            if (this.appels[i].idEvenement === appel.idEvenement) {
              this.appels[i] = appel;
              break;
            }
          }

            swal({
              type: 'success',
              title: 'appel mis à jour avec succes!',
              text: ' fermer'
            });
          this.message = 'Successfully updated!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  compare( rep1: Representant , rep2: Representant): boolean {
    return (rep1 && rep2 ) && (rep1.idRepresentant === rep2.idRepresentant);
  }

  ngAfterViewChecked() {
    if (this.isReady) {
      this.editorInit();
      this.isReady = false;
    }
  }



  editorInit() {
    let that = this;

    function ensureHTTP(str) {
      return /^https?:\/\//.test(str) && str || `http://${str}`;
    }
    if (document.getElementById('pellsappel') !== null && document.getElementById('pellsappel') !== undefined) {
      this.isChangeA = true;
      document.getElementById('pellsappel').innerHTML = '';
      const editor = pell.init({
        element: document.getElementById('pellsappel'),
        defaultParagraphSeparator: 'p',
        styleWithCSS: true,
        onChange(html) {
          that.appelTmp.resume = html;

        }
      });
      /* content init */
      editor.content.innerHTML = this.defaultContent;
    }


  }
/* pour ajouter un representant */
  addRepresentant() {
    if (!this.appelTmp.representant) {
      this.modal.open();
    }
  }
  affecter(rep: Representant) {
    this.appelTmp.representant = rep ;

  }
  add(event) {
    if (event.code === 'Enter'){
      this.addObejctif();
    }
  }


}
