import {
  AfterContentChecked,
  AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output,
  SimpleChanges, ViewChild
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Appel} from '../../../../domain/appel';
import {EvenementService} from '../../../../service/evenement.service';
import {Representant} from '../../../../domain/representant';
import pell from 'pell';
import {Mail} from '../../../../domain/mail';
import swal from 'sweetalert2';
import {FileUploader} from 'ng2-file-upload';
import {PieceJointe} from '../../../../domain/piece-jointe';
import {MediaService} from '../../../../service/media.service';
import {Entreprise} from '../../../../domain/entreprise';
import {fr} from '../../../../shared/global/fr';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';



@Component({
  selector: 'app-form-mail',
  templateUrl: './form-mail.component.html',
  styleUrls: ['./form-mail.component.scss']
})
export class FormMailComponent implements OnInit, AfterViewChecked, OnChanges {

  formMail: FormGroup;
  @Output()back: EventEmitter<Mail> = new EventEmitter();
  @Input()mail: Mail;
  @Input()representants: Representant[];
  @Input()isReady = false;
  message: string;
  @Input() defaultContent = '';
  @Input() isChangeM = false;
  @Input() id: number;
  @Input() mails: Mail [];
  @Input() entreprise: Entreprise ;
  @Input() type: string;
  mailTmp: Mail;
  isEntreprise = false;
  isProfessionel = false;
  obj = '';
  fr = fr;

  representant: Representant ;
  /*pour gérer l'ouverture du modal depuis le component */
  @ViewChild('representantForm') modal;

  public resume: string;

  constructor(private fB: FormBuilder , private evenementService: EvenementService, private mediaService: MediaService ) {
    this.mail = new Mail();
      this.formMail = this.fB.group(
      {
      'objectif': [''],
      'objetMail': ['', Validators.required],
      'lieuEvenement': [''],
      'contenu': [''],
      'dateEvenement': ['', Validators.required],
      'representant': [''],


    });
  }
  public  uploader: FileUploader = new FileUploader({ url: URL});

  ngOnInit() {
    this.representant = new Representant() ;
    if (this.type === 'entreprise') {
      this.isEntreprise = true;
    }

    if (this.type === 'personne') {
      this.isProfessionel = true;
    }
  }


  ngOnChanges(): void {
    if ( this.mail !== null || this.mail !== undefined) {
      this.mailTmp = JSON.parse(JSON.stringify(this.mail));
    }
  }
  public fileDropped(event: any) {
    const $this = this;
    for (let item of event){
      const media = new PieceJointe();
      let reader = new FileReader();
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        // recupérer base 64 dans media.url
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[1];
        if ($this.mailTmp.pieceJointes === null || $this.mailTmp.pieceJointes === undefined){
          $this.mailTmp.pieceJointes = [];
        }
        $this.mailTmp.pieceJointes.push(media);
      };
    }
  }


  onContentChange(event: string) {
  }

  addObejctif() {
    if (this.obj === '' || this.obj === undefined) { return ; }
    this.mailTmp.objectif.push(this.obj);

    this.obj = '';

  }
  add(event) {
    if (event.code === 'Enter') {
      this.addObejctif();
    }
  }
  deleteObj( obj) {
    for (let i = 0; i < this.mailTmp.objectif.length ; i++) {
      if (this.mailTmp.objectif[i] === obj) {
        this.mailTmp.objectif.splice(i, 1);
        break;
      }
    }

  }


  onSubmit() {
    if (this.obj !== null && this.obj !== undefined && this.obj !== '' ){
      this.addObejctif();
    }
    if (this.mail.idEvenement === undefined) {
      this.evenementService.creerMail(this.id , this.mailTmp).subscribe(
        mail => {
          this.back.emit(mail);
          this.mails.push(mail);
          this.message = 'Successfully created!';
          this.formMail.reset();
          this.obj = '';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          swal({
            type: 'success',
            title: 'Mail enregistré avec succes!',
            text: ' fermer'
          });
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.evenementService.updateMail(this.id, this.mailTmp).subscribe(
        mail => {
          this.back.emit(mail);
          for (let i = 0; i < this.mails.length; i++) {
            if (this.mails[i].idEvenement === mail.idEvenement) {
              this.mails[i] = mail;
              break;
            }
          }
          swal({
            type: 'success',
            title: 'mail mis à jour avec succes!',
            text: ' fermer'
          });
          this.message = 'Successfully updated!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }



  compare( rep1: Representant , rep2: Representant): boolean {
    return (rep1 && rep2 ) && (rep1.idRepresentant === rep2.idRepresentant);
}

  ngAfterViewChecked() {
    if (this.isReady) {
      this.editorInit();
      this.isReady = false;
    }
  }



  editorInit() {
    let that = this;

    function ensureHTTP(str) {
      return /^https?:\/\//.test(str) && str || `http://${str}`;
    }


    if (document.getElementById('pellsmail') !== null && document.getElementById('pellsmail') !== undefined) {
      this.isChangeM = true;
      document.getElementById('pellsmail').innerHTML = '';
      const editor = pell.init({
        element: document.getElementById('pellsmail'),
        actions: ['bold', 'italic', 'underline', 'heading1', 'heading2', 'olist', 'ulist', 'paragraph'],
        defaultParagraphSeparator: 'p',
        styleWithCSS: true,
        classes: {
          actionbar: 'pell-actionbar',
          button: 'pell-button',
          content: 'pell-content',
          selected: 'pell-button-selected'
        },
        onChange(html) {
          that.mailTmp.contenu = html;

        }
      });
      /* content init */
      editor.content.innerHTML = this.defaultContent;
    }

    }
  /* pour ajouter un representant */
  addRepresentant() {
    if (this.mailTmp.representant === null) {
      this.modal.open() ;
    }
  }
  affecter(rep: Representant) {
    this.mailTmp.representant = rep ;

  }





}
