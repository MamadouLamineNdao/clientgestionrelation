import {
  AfterContentChecked,
  AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output,
  SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Appel} from '../../../../domain/appel';
import {EvenementService} from '../../../../service/evenement.service';
import {Representant} from '../../../../domain/representant';
import pell from 'pell';
import {Reunion} from '../../../../domain/reunion';
import swal from 'sweetalert2';
import {PieceJointe} from '../../../../domain/piece-jointe';
import {FileUploader} from 'ng2-file-upload';
import {fr} from "../../../../shared/global/fr";
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';


@Component({
  selector: 'app-form-reunion',
  templateUrl: './form-reunion.component.html',
  styleUrls: ['./form-reunion.component.scss']
})
export class FormReunionComponent implements OnInit, AfterViewChecked, OnChanges {

  formReunion: FormGroup;
  @Output()back: EventEmitter<Reunion> = new EventEmitter();
  @Input()reunion: Reunion;
  @Input()isReady = false;
  message: string;
  @Input() defaultContent = '';
  @Input() isChangeR = false;
  @Input() id: number;
  @Input() reunions: Reunion[];
  reunionTmp: Reunion;
  pieceJointes: PieceJointe[] = [];
  fr = fr;
  obj: string;
  ord: string;



  public resume: string;

  constructor(private fB: FormBuilder , private evenementService: EvenementService) {
    this.reunion = new Reunion();
    this.formReunion = this.fB.group(
      {
      'objectif': [''],
      'lieuEvenement': ['', Validators.required],
      'compteRendu': [''],
      'dateEvenement': ['', Validators.required],


    });
  }
  public  uploader: FileUploader = new FileUploader({ url: URL});

  ngOnInit() {
  }

  ngOnChanges(): void {
    if ( this.reunion !== null || this.reunion !== undefined) {
      this.reunionTmp = JSON.parse(JSON.stringify(this.reunion));
    }
  }
  onContentChange(event: string) {
  }
  public fileDropped(event: any) {
    const $this = this;
    for (let item of event){
      const media = new PieceJointe();
      let reader = new FileReader();
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        // recupérer base 64 dans media.url
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[1];
        if ($this.reunionTmp.pieceJointes === null || $this.reunionTmp.pieceJointes === undefined) {
          $this.reunionTmp.pieceJointes = [];
        }
        $this.reunionTmp.pieceJointes.push(media);
      };
    }
  }

  onSubmit() {
    if (this.obj !== null && this.obj !== undefined && this.obj !== '' ) {
      this.addObejctif();
    }
    if (this.ord !== null && this.ord !== undefined && this.ord !== '' ) {
      this.addOrdreDuJour();
    }
    if (this.reunion.idEvenement === undefined) {
      this.evenementService.creerReunion(this.id , this.reunionTmp).subscribe(
        reunion => {
          this.back.emit(reunion);
          this.reunions.push(reunion);
          this.message = 'Successfully created!';
          this.formReunion.reset();
          this.ord = '';
          this.obj = '';
          setTimeout(
            () => this.message = undefined,
            2000
          );

          this.isChangeR = false;


          swal({
            type: 'success',
            title: 'Reunion enregistré avec succes!',
            text: ' fermer'
          });

        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.evenementService.updateReunion(this.id, this.reunionTmp).subscribe(
        reunion => {
          this.back.emit(reunion);
          for (let i = 0; i < this.reunions.length; i++) {
            if (this.reunions[i].idEvenement === reunion.idEvenement) {
              this.reunions[i] = reunion;
              break;
            }
          }
          swal({
            type: 'success',
            title: 'Reunion mis à jour avec succes!',
            text: ' fermer'
          });
          this.message = 'Successfully updated!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  addObejctif() {
    if (this.obj === '' || this.obj === undefined) { return ; }
    this.reunionTmp.objectif.push(this.obj);

    this.obj = '';

  }
  deleteObj( obj) {
    for (let i = 0; i < this.reunionTmp.objectif.length ; i++) {
      if (this.reunionTmp.objectif[i] === obj) {
        this.reunionTmp.objectif.splice(i, 1);
        break;
      }
    }

  }
addOrdreDuJour() {
    if (this.ord === '' || this.ord === undefined) { return ; }
    this.reunionTmp.ordreDuJour.push(this.ord);

    this.ord = '';

  }
  deleteOrdre( ord) {
    for (let i = 0; i < this.reunionTmp.ordreDuJour.length ; i++) {
      if (this.reunionTmp.ordreDuJour[i] === ord) {
        this.reunionTmp.ordreDuJour.splice(i, 1);
        break;
      }
    }

  }


  ngAfterViewChecked() {
    if (this.isReady) {
      this.isChangeR = true;

      if (document.getElementById('reunion') !== null && document.getElementById('reunion') !== undefined) {
        {
          this.isChangeR = true;
        }
      }
    }

  }
  /* ajout de l'objectif */
  add(event) {
    if (event.code === 'Enter') {
      this.addObejctif();
    }
  }
  addOrdJour(event) {
    if (event.code === 'Enter') {
      this.addOrdreDuJour();
    }
  }

}
