import {
  AfterContentChecked,
  AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output,
  SimpleChanges, ViewChild
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Appel} from '../../../../domain/appel';
import {EvenementService} from '../../../../service/evenement.service';
import {Representant} from '../../../../domain/representant';
import pell from 'pell';
import {Mail} from '../../../../domain/mail';
import swal from 'sweetalert2';
import {FileUploader} from 'ng2-file-upload';
import {PieceJointe} from '../../../../domain/piece-jointe';
import {MediaService} from '../../../../service/media.service';
import {Entreprise} from '../../../../domain/entreprise';
import {fr} from '../../../../shared/global/fr';
import {Officiel} from '../../../../domain/Officiel';
import {PartenaireService} from '../../../../service/partenaire.service';
import {ActivatedRoute, Router} from '@angular/router';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';



@Component({
  selector: 'app-form-officiel',
  templateUrl: './form-officiel.component.html',
  styleUrls: ['./form-officiel.component.scss']
})
export class FormOfficielComponent implements OnInit, AfterViewChecked, OnChanges {

  formMail: FormGroup;
  @Output()back: EventEmitter<Officiel> = new EventEmitter();
  @Input()officiel: Officiel;
  message: string;
  @Input() id: number;
  @Input() officiels: Officiel [];
  @Input() entreprise: Entreprise ;
  @Input() type: string;
  officielTmp: Officiel;
  isEntreprise = false;
  isProfessionel = false;
  obj = '';
  fr = fr;

  representant: Representant ;
  /*pour gérer l'ouverture du modal depuis le component */


  constructor( private partenaireService: PartenaireService, private mediaService: MediaService, private route: Router ) {


  }
  public  uploader: FileUploader = new FileUploader({ url: URL});

  ngOnInit() {
    if (this.type === 'entreprise') {
      this.isEntreprise = true;
    }

    if (this.type === 'personne') {
      this.isProfessionel = true;
    }
  }


  ngOnChanges(): void {
    if ( this.officiel !== null && this.officiel !== undefined) {
      this.officielTmp = JSON.parse(JSON.stringify(this.officiel));
    }
  }
  public fileDropped(event: any) {
    const $this = this;
    for (let item of event){
      const media = new PieceJointe();
      let reader = new FileReader();
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        // recupérer base 64 dans media.url
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[1];
        if ($this.officielTmp.pieceJointes === null || $this.officielTmp.pieceJointes === undefined){
          $this.officielTmp.pieceJointes = [];
        }
        $this.officielTmp.pieceJointes.push(media);
      };
    }
  }


  onContentChange(event: string) {
    console.log(event);
  }



  onSubmit() {
    if (this.officiel.idOfficiel === undefined) {
      this.partenaireService.officialiserPartenaire(this.officielTmp , this.id).subscribe(
        officiel => {
          this.back.emit(officiel);
          this.message = 'Successfully created!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.route.navigate(['pages/partenaire/officiel']);


        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.partenaireService.updateOfficiel(this.officielTmp).subscribe(
        officiel => {
          this.back.emit(officiel);
          for (let i = 0; i < this.officiels.length; i++) {
            if (this.officiels[i].idOfficiel === officiel.idOfficiel) {
              this.officiels[i] = officiel;
              break;
            }
          }
          swal({
            title: 'officiel mis à jour avec succes!',
            text: 'fermeture dans 2 seconds.',
            timer: 2000,
            onOpen: () => {
              swal.showLoading();
            }
          }).then((result) => {

            if (result.dismiss.toString() === 'timer') {
              console.log('I was closed by the timer');
            }
          });
          this.message = 'Successfully updated!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }




  ngAfterViewChecked() {

  }




  /* pour ajouter un representant
  addRepresentant() {
    if (this.mailTmp.representant === null) {
      this.modal.open() ;
    }
  }
  affecter(rep: Representant) {
    this.mailTmp.representant = rep ;

  }
  */





}
