
import {
  AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, Input, OnInit,
  Output
} from "@angular/core";
import {Partenaire} from "../../../domain/partenaire";
import {Intermediaire} from "../../../domain/intermediaire";
import {Personne} from "../../../domain/personne";
import {Entreprise} from "../../../domain/entreprise";
import {ActivatedRoute, Params, Router} from "@angular/router";
import swal from 'sweetalert2';
import {forEach} from "@angular/router/src/utils/collection";
import {Evaluation} from "../../../domain/evaluation";
import {EvenementService} from "../../../service/evenement.service";
import {Appel} from "../../../domain/appel";
import {Mail} from "../../../domain/mail";
import {Reunion} from "../../../domain/reunion";
import {PartenaireService} from "../../../service/partenaire.service";
import {Representant} from "../../../domain/representant";
declare var $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [PartenaireService]
})
export class DetailComponent implements OnInit, AfterViewChecked {



  @Input() defaultContent;





  constructor( private route: ActivatedRoute , router: Router , private partenaireService: PartenaireService ) { }

  ngOnInit() {}




  ngAfterViewChecked(): void {

  }
}
