


import {AfterViewInit, Component, Input, OnInit} from "@angular/core";
import {AgendaService} from "../../../../service/agenda.service";
declare var $: any;



@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit, AfterViewInit {
@Input() dateEvenement: number;

  constructor(private agendaService: AgendaService ) {
  }
  ngOnInit() {
  }
  notifier() {

  }
  ngAfterViewInit(): void {
    let $this = this;
    let lunch = new Date(2018, 11 , 30 , 21 , 20 , 0 );
    let notif;
    let Days = $('#day');
    let Hours = $('#heures');
    let Minute = $('#minutes');
    let second = $('#second');
    let stop = false;
    setDate();
    function setDate() {
      if (stop) {

        Days.html('<strong> ' + 0 + ' </strong>Jour' );
        Hours.html('<strong> ' + 0 + ' </strong>Heure'  );
        Minute.html('<strong> ' + 0 + ' </strong>Minute' );
        second.html('<strong> ' + 0 + ' </strong>Second' );



        return ; }

      let now = new Date();
      let s = (lunch.getTime() - now.getTime()) / 1000;

      notif = (lunch.getTime() - (now.getTime() + 5 * 60 ));


      let d = Math.floor(s / 86400);
      Days.html('<strong> ' + d + ' </strong>Jour' + (d > 1 ? 's' : ' ') );
      s -= d * 86400;

      let h = Math.floor(s / 3600);
      Hours.html('<strong> ' + h + ' </strong>Heure' + (h > 1 ? 's' : '') );
      s -= h * 3600;


      let m = Math.floor(s / 60);
      Minute.html('<strong> ' + m + ' </strong>Minute' + (m > 1 ? 's' : '') );
      s -= m * 60;


      s = Math.floor(s);
      second.html('<strong> ' + s + ' </strong>Second' + (s > 1 ? 's' : '') );

      if (notif === 0) {

      }
      setTimeout(setDate, 1000);
      if (d === 0 && h === 0 && s === 0 || d < 0) {
        stop = true;
      }

    }
  }
}
