
import {PartenaireService} from '../../../service/partenaire.service';
import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Partenaire} from '../../../domain/partenaire';
import {Intermediaire} from '../../../domain/intermediaire';
import {Personne} from '../../../domain/personne';
import {Entreprise} from '../../../domain/entreprise';
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert2';
import {Evaluation} from '../../../domain/evaluation';
import {Domaine} from '../../../domain/domaine';
import {Representant} from '../../../domain/representant';
import {isUndefined} from 'util';
import {noUndefined} from '@angular/compiler/src/util';
import {Appel} from '../../../domain/appel';
import {EvenementService} from '../../../service/evenement.service';
import {Mail} from '../../../domain/mail';
import {Reunion} from '../../../domain/reunion';
import {RepresentantService} from '../../../service/representant.service';
import {Evenement} from "../../../domain/evenement";
import {MediaService} from "../../../service/media.service";
import {AgendaService} from "../../../service/agenda.service";
import {Tache} from "../../../domain/tache";
import {Officiel} from '../../../domain/Officiel';
declare var $: any;


@Component({
  selector: 'app-officiel-partenaire',
  templateUrl: './partenaire-officiel.component.html',
  styleUrls: ['./partenaire-officiel.component.scss'],
  providers: [PartenaireService, EvenementService, RepresentantService, MediaService, AgendaService]
})
export class PartenaireOffiielComponent implements OnInit, AfterViewInit {


  order ;
  decor = 'tic';
  reverse;
  filtercolumn: string[] ;
  filter: string ;
  appels: Appel[];
  officielEntreprises: Officiel[];
  officielPersonnes: Officiel[];
  type: string;
  isEntreprise = false;
  isProfessionel = false;
  officiel: Officiel;
  modal: any;


  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  isReady = false;

  constructor( private partenaireService: PartenaireService , private route: ActivatedRoute ,
               private router: Router, private evenementService: EvenementService, private representantService: RepresentantService) { }



  openSite(link: string) {
    if (link) {
      if (link.indexOf('http') === 0) {
        return link;
      }
      return 'http://' + link;
    }
  }

  // qui donne le statut de l'appel



  ngOnInit() {
    this.type = this.route.snapshot.paramMap.get('type');
    if (this.type === 'entreprise' ) {
      this.isEntreprise = true;
      /* */
    }
    if (this.type === 'personne') {
      this.isProfessionel = true;
    }
    this.partenaireService.listeOfficielEntreprise().subscribe(
      data => {
        this.officielEntreprises = data;
      },
      error1 => {
        console.log('erreur sur la requete pour avoir liste officiel');
      }

    );
    this.partenaireService.listeOfficielPersonne().subscribe(
      data => {
        this.officielPersonnes = data;
      },
      error1 => {
        console.log('erreur sur la requete pour avoir liste officiel');
      }

    );
  }
  goInfo(id: number, type: string){
    this.router.navigate(['/pages/partenaire/intermediaire/detail', id, type], { queryParams: { etat: 'officiel' } });
  }




  closeParent(modal: any) {
    modal.close();
    console.log('closeParent') ;

  }











  closeModal(modal) {
    modal.close();
  }

  onClose() {
  }


  onDelete(officiel: Officiel) {
    swal({
      title: 'Vous etes sure?',
      text: 'Vous allez supprimer definitivement!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, supprimer!',
      cancelButtonText: 'No, annuler'
    }).then((result) => {
      if (result.value) {
        this.partenaireService.deleteOfficiel(officiel).subscribe(
          () => {
            swal(
              'supprimer!',
              'Votre partenaire a été supprimer avec succes.',
              'success'
            );

            for (let i = 0; i < this.officielEntreprises.length ; i++) {
              if (this.officielEntreprises[i].idOfficiel === officiel.idOfficiel) {
                this.officielEntreprises.splice(i, 1);
                break;
              }
            }
            for (let i = 0; i < this.officielPersonnes.length ; i++) {
              if (this.officielPersonnes[i].idOfficiel === officiel.idOfficiel) {
                this.officielPersonnes.splice(i, 1);
                break;
              }
            }



          }, error =>
            swal(
              'Cancelled',
              'Your imaginary file is safe :)',
              'error'
            )
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'annuler',
          'Your imaginary file is safe :)',
          'error'
        );
      }
    });
  }

  openModal(modal: any , even: any) {
    console.log('###########################################################################')
    this.modal = modal;
    this.officiel = even;


    modal.open();
  }



// pour le filter

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }
  filterDefineColumn() {
    if (this.filter === null || this.filter === undefined || this.filter === '' ) {
      this.filtercolumn = [] ;
    }else {
      this.filtercolumn = [this.filter, 'nomEntreprise', 'dateValidationSuggestion', 'sousDomaines', 'tailleEntreprise.min' ] ;
    }
  }






  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }
ngAfterViewInit() {
  let x = location.pathname;
  console.log(x);

  let page = x.indexOf('appel');
  if (page !== -1) {
    console.log('sa contient appel');
    // window.location.hash = 'localities';
    $('html,body').animate({scrollTop: $('#description' ).offset().top}, 'lows');


  }


  // let chemin = $(location).attr(pathname);
  let searchVisible = 0;
  let transparent = true;
  /*  Activate the tooltips      */
  $('[rel="tooltip"]').tooltip();

  // Code for the Validator

  // Wizard Initialization
  $('.wizard-card').bootstrapWizard({
    'tabClass': 'nav nav-pills',
    'nextSelector': '.btn-next',
    'previousSelector': '.btn-previous',


    onInit : function(tab, navigation, index){

      let $total = navigation.find('li').length;
      let $width = 100 / $total;

      navigation.find('li').css('width', $width + '%');


    },


    onTabShow: function(tab, navigation, index) {
      let $total = navigation.find('li').length;
      let $current = index + 1;

      let $wizard = navigation.closest('.wizard-card');
      // If it's the last tab then hide the last button and show the finish instead
      if ($current >= $total) {
        $($wizard).find('.btn-next').hide();
        $($wizard).find('.btn-finish').show();
      } else {
        $($wizard).find('.btn-next').show();
        $($wizard).find('.btn-finish').hide();
      }
      let move_distance = 100 / $total;
      move_distance = move_distance * (index) + move_distance / 2;
      console.log(index);
      $wizard.find($('.progress-bar')).css({width: move_distance + '%'});


      $wizard.find($('.wizard-card .nav-pills li.active a .icon-circle')).addClass('checked');

    }
  });


  // Prepare the preview for profile picture
  /* $('#wizard-picture').change(function(){
     this.readURL(this);
   });*/
  /*
      $('[data-toggle="wizard-radio"]').click(function(){
        let wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', true);
      });*/

  /* $('[data-toggle="wizard-checkbox"]').click(function(){
     if ($(this).hasClass('active')) {
       $(this).removeClass('active');
       $(this).find('[type="checkbox"]').removeAttr('checked');
     } else {
       $(this).addClass('active');
       $(this).find('[type="checkbox"]').attr('checked', true);
     }
   });*/

  $('.set-full-height').css('height', 'auto');
}

}
