
import {PartenaireService} from '../../../service/partenaire.service';
import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Partenaire} from '../../../domain/partenaire';
import {Intermediaire} from '../../../domain/intermediaire';
import {Personne} from '../../../domain/personne';
import {Entreprise} from '../../../domain/entreprise';
import {ActivatedRoute, Params, Router} from '@angular/router';
import swal from 'sweetalert2';
import {Evenement} from "../../../domain/evenement";
import {EvenementService} from "../../../service/evenement.service";
import {RepresentantService} from "../../../service/representant.service";
import {AgendaService} from "../../../service/agenda.service";
import {noUndefined} from "@angular/compiler/src/util";
import {Appel} from "../../../domain/appel";
import {Reunion} from "../../../domain/reunion";
import {Mail} from "../../../domain/mail";
declare var $: any;


@Component({
  selector: 'app-entreprise',
  templateUrl: './partenaire-entreprise.component.html',
  styleUrls: ['./partenaire-entreprise.component.scss'],
  providers: [PartenaireService, EvenementService, RepresentantService, AgendaService]
})
export class PartenaireEntrepriseComponent implements OnInit, AfterViewInit {

  /*filtrage*/
  filtercolumn: string[] ;
  filter: string ;

  partenaires: Partenaire[] = [];
  intermediairesEnt: Intermediaire[] = [];
  intermediaires: Intermediaire[] = [];
  intermediairesPer: Intermediaire[] = [];
  personnes: Personne[] = [];
  evenements: Evenement[] = [];
  entreprises: Entreprise[] = [];
  entrepriseDetail: Entreprise;
  personneDetail: Personne;
  id: string;
  type: string;
  isEntreprise = false;
  isProfessionel = false;
  @Input()
  personne: Personne ;
  @Input()
  @Output() back = new EventEmitter() ;
  @Output() delete = new EventEmitter() ;

  tableData: Array<any>;

  /* pagination Info */
  pageSize = 3;
  pageNumber = 1;

  constructor(private partenaireService: PartenaireService ,
              private router: Router , private route: ActivatedRoute,
              private evenementService: EvenementService) { }

  ngOnInit() {
    let dateYear = new Date();
    this.partenaireService.listeIntermediairePersonne().subscribe(
      data => {this.intermediairesPer = data;
      },
      error => {console.log('An error was occured.'); }
    );
    this.evenementService.listeEvenement().subscribe(
      data => {
        for (let i = 0 ; i < data.length ; i++) {
          this.evenementService.EvenementTypeAppel(data[i].idEvenement).subscribe(
            appel => {
              if (appel !== null) {
                data[i].type = 'Appel';
              }
            },
            error2 => {
            }
          );

          this.evenementService.EvenementTypeMail(data[i].idEvenement).subscribe(
            mail => {
              if (mail !== null) {
                data[i].type = 'Mail';
              }
            },
            error2 => {
            }
          );

          this.evenementService.EvenementTypeReunion(data[i].idEvenement).subscribe(
            reunion => {
              if (reunion !== null) {
                data[i].type = 'Reunion';
              }
            },
            error2 => {
            }
          );

          this.evenementService.intermediaireEvenement(data[i].idEvenement).subscribe(
            intermediaire => {
              data[i].intermediaire = intermediaire;
            },
            error => {console.log('An error was occured.'); }
          );
        }

        this.evenements = data;
      //  console.log(this.evenements);
      },
      error => {console.log('An error was occured.'); }
    );


    this.partenaireService.listeIntermediaireEntreprise().subscribe(
      data => {this.intermediairesEnt = data;

      },
      error => {console.log('An error was occured.'); }
    );
    this.loadData();

  }

  onDelete(intermediaire) {
    swal({
      title: 'Vous etes sure?',
      text: 'Vous allez supprimer definitivement ce partenaire!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'supprimer!',
      cancelButtonText: 'annuler'
    }).then((result) => {
      if (result.value) {
        this.partenaireService.delete(intermediaire).subscribe(
          () => {
            swal(
              'supprimer!',
              'Votre partenaire a été supprimer avec succes.',
              'success'
            );
            for (let i = 0; i < this.intermediairesEnt.length ; i++) {
              if (this.intermediairesEnt[i].idPartenaire === intermediaire.idPartenaire) {
                this.intermediairesEnt.splice(i, 1);
                break;
              }
            }
            for (let i = 0; i < this.intermediairesPer.length ; i++) {
              if (this.intermediairesPer[i].idPartenaire === intermediaire.idPartenaire) {
                this.intermediairesPer.splice(i, 1);
                break;
              }
            }

          }, error =>
            swal(
              'annuler',
              'Veuillez supprimer les événement avant de le supprimer',
              'error'
            )
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'annuler',
          'suppression annuler',
          'error'
        );
      }
    });
  }

  getIntermediaireType(value: string) {
    if (value === undefined || value === null) {
      this.isProfessionel = true;
      return ;
    }
    this.isEntreprise = true;

  }

  getTypeEvenement(even: Evenement) {

  }





  loadData() {
    this.tableData = this.partenaireService.intermediaires;
  }


  onDetail(id: number, type: string): void {
    this.router.navigate(['./detail', id, type], {relativeTo: this.route });
  }

  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }
  ngAfterViewInit(): void {
    let x = location.pathname;

    let page = x.indexOf('appel');
    if (page !== -1) {
      // window.location.hash = 'localities';
      $('html,body').animate({scrollTop: $('#description' ).offset().top}, 'lows');


    }


    // let chemin = $(location).attr(pathname);
    let searchVisible = 0;
    let transparent = true;
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    // Code for the Validator

    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'nextSelector': '.btn-next',
      'previousSelector': '.btn-previous',


      onInit : function(tab, navigation, index){

        let $total = navigation.find('li').length;
        let $width = 100 / $total;

        navigation.find('li').css('width', $width + '%');


      },


      onTabShow: function(tab, navigation, index) {
        let $total = navigation.find('li').length;
        let $current = index + 1;

        let $wizard = navigation.closest('.wizard-card');
        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }
        let move_distance = 100 / $total;
        move_distance = move_distance * (index) + move_distance / 2;
        $wizard.find($('.progress-bar')).css({width: move_distance + '%'});


        $wizard.find($('.wizard-card .nav-pills li.active a .icon-circle')).addClass('checked');

      }
    });


    // Prepare the preview for profile picture
    /* $('#wizard-picture').change(function(){
       this.readURL(this);
     });*/
    /*
        $('[data-toggle="wizard-radio"]').click(function(){
          let wizard = $(this).closest('.wizard-card');
          wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
          $(this).addClass('active');
          $(wizard).find('[type="radio"]').removeAttr('checked');
          $(this).find('[type="radio"]').attr('checked', true);
        });*/

    /* $('[data-toggle="wizard-checkbox"]').click(function(){
       if ($(this).hasClass('active')) {
         $(this).removeClass('active');
         $(this).find('[type="checkbox"]').removeAttr('checked');
       } else {
         $(this).addClass('active');
         $(this).find('[type="checkbox"]').attr('checked', true);
       }
     });*/

    $('.set-full-height').css('height', 'auto');

  }
  filterDefineColumnPersonne() {
    if (this.filter === null || this.filter === undefined || this.filter === '' ) {
      this.filtercolumn = [] ;
    }
    else {
      this.filtercolumn = [this.filter, 'cible.nom', 'cible.prenom', 'cible.profession', 'cible.telephone'] ;

    }
  }
  filterDefineColumnEntreprise() {
    if (this.filter === null || this.filter === undefined || this.filter === '' ) {
      this.filtercolumn = [] ;
    }
    else {
      this.filtercolumn = [this.filter, 'cible.nomEntreprise', 'cible.dateValidationSuggestion', 'cible.sousDomaines', 'cible.tailleEntreprise.min' ] ;

    }
  }
}
