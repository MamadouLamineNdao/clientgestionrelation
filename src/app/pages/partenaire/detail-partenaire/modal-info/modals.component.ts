import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import swal from 'sweetalert2';
declare var $: any

@Component({
  selector: 'app-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.scss']
})

export class ModalsComponent implements OnInit, AfterViewInit {


  constructor() { }

  ngOnInit() { }

  openModal(modal) {
    modal.open();
  }

  closeModal(modal) {
    modal.close();
  }

  onClose() {
    swal({
      type: 'success',
      title: 'Success!',
      text: 'close it!',
    });
  }

  ngAfterViewInit(): void {
    let searchVisible = 0;
    let transparent = true;
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    // Code for the Validator
    let $validator = $('.wizard-card form').validate({
      rules: {
        firstname: {
          required: true,
          minlength: 3
        },
        lastname: {
          required: true,
          minlength: 3
        },
        email: {
          required: true
        }
      },
    });

    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'nextSelector': '.btn-next',
      'previousSelector': '.btn-previous',

      onNext: function(tab, navigation, index) {
        let $valid = $('.wizard-card form').valid();
        if (!$valid) {
          $validator.focusInvalid();
          return false;
        }
      },

      onInit : function(tab, navigation, index){

        let $total = navigation.find('li').length;
        let $width = 100 / $total;

        navigation.find('li').css('width', $width + '%');

      },

      onTabClick : function(tab, navigation, index){

        let $valid = $('.wizard-card form').valid();

        if (!$valid) {
          return false;
        } else {
          return true;
        }

      },

      onTabShow: function(tab, navigation, index) {
        let $total = navigation.find('li').length;
        let $current = index + 1;

        let $wizard = navigation.closest('.wizard-card');

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }


        let move_distance = 100 / $total;
        move_distance = move_distance * (index) + move_distance / 2;

        $wizard.find($('.progress-bar')).css({width: move_distance + '%'});


        $wizard.find($('.wizard-card .nav-pills li.active a .icon-circle')).addClass('checked');

      }
    });


    // Prepare the preview for profile picture
    $('#wizard-picture').change(function(){
      this.readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function(){
      let wizard = $(this).closest('.wizard-card');
      wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
      $(this).addClass('active');
      $(wizard).find('[type="radio"]').removeAttr('checked');
      $(this).find('[type="radio"]').attr('checked', true);
    });

    $('[data-toggle="wizard-checkbox"]').click(function(){
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
      } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', true);
      }
    });

    $('.set-full-height').css('height', 'auto');

  }
}
