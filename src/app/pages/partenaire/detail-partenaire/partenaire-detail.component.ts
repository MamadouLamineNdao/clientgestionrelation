
import {PartenaireService} from '../../../service/partenaire.service';
import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Partenaire} from '../../../domain/partenaire';
import {Intermediaire} from '../../../domain/intermediaire';
import {Personne} from '../../../domain/personne';
import {Entreprise} from '../../../domain/entreprise';
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert2';
import {Evaluation} from '../../../domain/evaluation';
import {Domaine} from '../../../domain/domaine';
import {Representant} from '../../../domain/representant';
import {isUndefined} from 'util';
import {noUndefined} from '@angular/compiler/src/util';
import {Appel} from '../../../domain/appel';
import {EvenementService} from '../../../service/evenement.service';
import {Mail} from '../../../domain/mail';
import {Reunion} from '../../../domain/reunion';
import {RepresentantService} from '../../../service/representant.service';
import {Evenement} from "../../../domain/evenement";
import {MediaService} from "../../../service/media.service";
import {AgendaService} from "../../../service/agenda.service";
import {Tache} from "../../../domain/tache";
import {Officiel} from '../../../domain/Officiel';
declare var $: any;


@Component({
  selector: 'app-detail-partenaire',
  templateUrl: './partenaire-detail.component.html',
  styleUrls: ['./partenaire-detail.component.scss'],
  providers: [PartenaireService, EvenementService, RepresentantService, MediaService, AgendaService]
})
export class PartenaireDetailComponent implements OnInit, AfterViewInit {

  intermediaires: Intermediaire[] = [];
  personnes: Personne[] = [];
  representants: Representant[]= [];
  dataIntermediaire: Intermediaire;
  evaluationAxis: Date[]= [];
  evaluationOrd: number[]= [];
  evaluat: Evaluation[]= [];
  personneDetail: Personne;
  officielEntreprise: Officiel;
  officielPersonne: Officiel;
  isOfficiel = false;
  appel: Appel ;
  mail: Mail ;
  tache: Tache;
  officiel: Officiel;
  representant: Representant ;
  reunion: Reunion ;
  modal ;
  entrepriseDetail: Entreprise;
  type: string;
  id: string;
  i: number;
  dataReadyA = false;
  dataReadyM = false;
  dataReadyR = false;
  showloading= false;
  isValid = false;
  isOpened = false;
  isChange = false;
  isEntreprise = false;
  isProfessionel = false;
  appels: Appel [] = [];
  mails: Mail [] = [];
  reunions: Reunion [] = [];
  defaultContent= '';
  order ;
  decor = 'tic';
  reverse;
  filtercolumn: string[] ;
  filter: string ;
  dataAppel: Appel[] = [];
  dataReunion: Reunion[] = [];
  dataMail: Mail[] = [];
  taches: Tache[] = [];
  dateYearCourante = new Date();
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [ 'janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
  public barChartType = 'bar';
  public barChartLegend = true;

  set1: any ;


  public barChartData: any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Appels'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Mails'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Reunions'}
  ];

  tableData: Array<any>;

  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  isReady = false;
  value: any = [];
  etatPartenaire: string;

  constructor( private partenaireService: PartenaireService ,
               private route: ActivatedRoute ,
               private router: Router,
               private evenementService: EvenementService,
               private representantService: RepresentantService) {

  }

  findYear(id: string , value: number): void {
    // Only Change 3 values
    this.evenementService.appelChart(id , this.dateYearCourante.getFullYear()).subscribe(
      result => {
        const data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (let i = 0 ; i < result.length ; i++) {
          let date = new Date(result[i].dateEvenement);
          switch (date.getMonth()) {
            case 0: {data[0]++; break; }
            case 1: {data[1]++; break; }
            case 2: {data[2]++; break; }
            case 3: {data[3]++; break; }
            case 4: {data[4]++; break; }
            case 5: {data[5]++; break; }
            case 6: {data[6]++; break; }
            case 7: {data[7]++; break; }
            case 8: {data[8]++; break; }
            case 9: {data[9]++; break; }
            case 10: {data[10]++; break; }
            case 11: {data[11]++; break; }
          }
        }
        this.barChartData[0].data = data;
        this.dataReadyA = true;
      }
    );

    this.evenementService.mailChart(id, this.dateYearCourante.getFullYear()).subscribe(
      result => {
        const data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (let i = 0 ; i < result.length ; i++) {
          let date = new Date(result[i].dateEvenement);
          switch (date.getMonth()) {
            case 0: {data[0]++; break; }
            case 1: {data[1]++; break; }
            case 2: {data[2]++; break; }
            case 3: {data[3]++; break; }
            case 4: {data[4]++; break; }
            case 5: {data[5]++; break; }
            case 6: {data[6]++; break; }
            case 7: {data[7]++; break; }
            case 8: {data[8]++; break; }
            case 9: {data[9]++; break; }
            case 10: {data[10]++; break; }
            case 11: {data[11]++; break; }
          }
        }
        this.barChartData[1].data = data;
        this.dataReadyM = true;
      }
    );

    this.evenementService.reunionChart(id , this.dateYearCourante.getFullYear()).subscribe(
      result => {
        const data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (let i = 0 ; i < result.length ; i++) {
          let date = new Date(result[i].dateEvenement);
          switch (date.getMonth()) {
            case 0: {data[0]++; break; }
            case 1: {data[1]++; break; }
            case 2: {data[2]++; break; }
            case 3: {data[3]++; break; }
            case 4: {data[4]++; break; }
            case 5: {data[5]++; break; }
            case 6: {data[6]++; break; }
            case 7: {data[7]++; break; }
            case 8: {data[8]++; break; }
            case 9: {data[9]++; break; }
            case 10: {data[10]++; break; }
            case 11: {data[11]++; break; }
          }
        }
        this.barChartData[2].data = data;
        this.dataReadyR = true;
      }
    );
  }

  openSite(link: string) {
    if (link) {
      if (link.indexOf('http') === 0) {
        return link;
      }
      return 'http://' + link;
    }
  }

  // qui donne le statut de l'appel

  getstatut(value: boolean) {
    if (value === true) { return 'entrant'; }
    if (value === false) { return 'sortant'; }

  }
  getStatutPartenaire(value: boolean) {
    if (value === true) {
      return 'officiel';
    }
    if (value === false) {
      return 'visé';
    }
  }
  /*
  getHeader(value: boolean){
    if (this.isProfessionel) {
      return
    }

  }*/

  ngOnInit() {
    this.appel = new Appel();
    this.reunion = new Reunion();
    this.mail = new Mail();
    this.representant = new Representant();
    this.id = this.route.snapshot.paramMap.get('id');
    this.type = this.route.snapshot.paramMap.get('type');
    this.route.queryParamMap.subscribe(params => {
      this.etatPartenaire = params.get('etat');
      if (this.etatPartenaire === 'officiel') {
        this.partenaireService.getOfficielByIntermediaire(this.id).subscribe(
          value1 => {
            this.isOfficiel = true;
            if (this.type === 'entreprise') {
              this.officielEntreprise = value1;
            }
            if (this.type === 'personne') {
              this.officielPersonne = value1;
            }
          }
        );
      }
    });
    this.findYear(this.id, 2018);
    this.tache = new Tache();

    if (this.type === 'entreprise' ) {
    this.isEntreprise = true;
    this.partenaireService.detailEntrepise(+this.id).subscribe(
      data => {this.entrepriseDetail = data;
        for (const sousDomaine of this.entrepriseDetail.sousDomaines) {
          const item = {
            id: sousDomaine.idSousDomaine,
            text: sousDomaine.libelle
          };
          this.value.push(item);
        }
      },
      error => {console.log('An error was occured.'); }
    );
        this.isValid = true;
      /* */
      }
    if (this.type === 'personne') {
    this.isProfessionel = true;
    this.partenaireService.detailPersonne(+this.id).subscribe(
      data => {this.personneDetail = data;

      },
      error => {console.log('An error was occured'); }
    );
      }


    this.evenementService.listeTache().subscribe(
      data => {
        this.taches = data;
      },
      error => {console.log('An error was occured.'); }

    );

    this.partenaireService.listeAppel(+this.id).subscribe(
      data => {this.appels = data;
      },
      error => {console.log('An error was occured.'); }
    );
    this.partenaireService.listeReunion(+this.id).subscribe(
      data => {this.reunions = data;
      },
      error => {console.log('An error was occured.'); }
    );
    this.partenaireService.listeMail(+this.id).subscribe(
      data => {this.mails = data;
      //  console.log(this.mails);
      },
      error => {console.log('An error was occured.'); }
    );

  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  closeParent(modal: any) {
    modal.close();

  }

  addApp(modal: any) {
    let app: Appel = new Appel();
    this.openModal(modal , app , 'appel');
  }
  addmail(modal: any) {
    this.openModal(modal , new Mail(), 'mail');
  }
  addreunion(modal: any) {
    this.openModal(modal , new Reunion(), 'reunion');
  }
  addRepresentant(modal: any) {
    this.openModal( modal , new Representant(), 'rep');
  }
  addTache(modal: any) {
    this.openModal(modal , new Tache() , 'tache');
  }

  addOfficiel(modal: any) {
    this.openModal(modal, new Officiel(), 'officiel');
  }





  openModal(modal: any , even: any, type: string) {
    this.isOpened = true;
    this.modal = modal;
    if (type === 'rep') {
      this.representant = even;
    }
    if (type === 'mail') {
      even.dateEvenement = new Date(even.dateEvenement);
      this.mail = even;
      this.defaultContent = even.contenu || '';

    }
    if (type === 'appel') {
      even.dateEvenement = new Date(even.dateEvenement);

      this.appel = even;
      this.defaultContent = even.resume || '';
    }
    if (type === 'reunion') {
      even.dateEvenement = new Date(even.dateEvenement);
      this.reunion = even;
      this.defaultContent = even.compteRendu || '';

    }
    if (type === 'officiel') {
      this.officiel = even;
    }

    if (type === 'tache') {
      even.dateTache = new Date(even.dateTache);
      this.tache = even;


    }
    modal.open();
  }

  openText(modal: any, defaultContent: string) {
    this.modal = modal;
    this.defaultContent = defaultContent;
    if (defaultContent === '') {this.defaultContent = ''; }
    modal.open();
  }


  closeModal(modal) {
    modal.close();
  }

  onClose() {
    this.isOpened = false;
    this.isChange = false;
  }


  onDelete(id , evenement) {
    swal({
      title: 'Vous etes sure?',
      text: 'Vous allez supprimer definitivement!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'supprimer!',
      cancelButtonText: 'annuler'
    }).then((result) => {
      if (result.value) {
        this.evenementService.delete(id , evenement).subscribe(
          () => {
            swal(
              'supprimer!',
              'Votre partenaire a été supprimer avec succes.',
              'success'
            );

            for (let i = 0; i < this.appels.length ; i++) {
              if (this.appels[i].idEvenement === evenement.idEvenement) {
                this.appels.splice(i, 1);
                break;
              }
            }
            for (let i = 0; i < this.mails.length ; i++) {
              if (this.mails[i].idEvenement === evenement.idEvenement) {
                this.mails.splice(i, 1);
                break;
              }
            }
            for (let i = 0; i < this.reunions.length ; i++) {
              if (this.reunions[i].idEvenement === evenement.idEvenement) {
                this.reunions.splice(i, 1);
                break;
              }
            }


          }, error =>
            swal(
              'annuler',
              'suppression annuler',
              'error'
            )
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'annuler',
          'suppression annuler',
          'error'
        );
      }
    });
  }

  onDeleteRep(id , representant) {
    swal({
      title: 'Vous etes sure?',
      text: 'Vous allez supprimer un representant definitivement!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, supprimer!',
      cancelButtonText: 'No, annuler'
    }).then((result) => {
      if (result.value) {
        this.representantService.delete(id , representant).subscribe(
          () => {
            swal(
              'supprimer!',
              'Votre representant a été supprimer avec succes.',
              'success'
            );

            for (let i = 0; i < this.entrepriseDetail.representants.length ; i++) {
              if (this.entrepriseDetail.representants[i].idRepresentant === representant.idRepresentant) {
                this.entrepriseDetail.representants.splice(i, 1);
                break;
              }
            }



          }, error =>
            swal(
              'annuler',
              'Ce representant est lié a des événement',
              'error'
            )
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'annuler',
          'Your imaginary file is safe :)',
          'error'
        );
      }
    });
  }


// pour le filter

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }
  filterDefineColumn() {
    if (this.filter === null || this.filter === undefined || this.filter === '' ) {
      this.filtercolumn = [] ;
    }else {
      this.filtercolumn = [this.filter, 'nomEntreprise', 'dateValidationSuggestion', 'sousDomaines', 'tailleEntreprise.min' ] ;
    }
  }






  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }
ngAfterViewInit() {
  let x = location.pathname;

  let page = x.indexOf('appel');
  if (page !== -1) {
    // window.location.hash = 'localities';
    $('html,body').animate({scrollTop: $('#description' ).offset().top}, 'lows');


  }


  // let chemin = $(location).attr(pathname);
  let searchVisible = 0;
  let transparent = true;
  /*  Activate the tooltips      */
  $('[rel="tooltip"]').tooltip();

  // Code for the Validator

  // Wizard Initialization
  $('.wizard-card').bootstrapWizard({
    'tabClass': 'nav nav-pills',
    'nextSelector': '.btn-next',
    'previousSelector': '.btn-previous',


    onInit : function(tab, navigation, index){

      let $total = navigation.find('li').length;
      let $width = 100 / $total;

      navigation.find('li').css('width', $width + '%');


    },


    onTabShow: function(tab, navigation, index) {
      let $total = navigation.find('li').length;
      let $current = index + 1;

      let $wizard = navigation.closest('.wizard-card');
      // If it's the last tab then hide the last button and show the finish instead
      if ($current >= $total) {
        $($wizard).find('.btn-next').hide();
        $($wizard).find('.btn-finish').show();
      } else {
        $($wizard).find('.btn-next').show();
        $($wizard).find('.btn-finish').hide();
      }
      let move_distance = 100 / $total;
      move_distance = move_distance * (index) + move_distance / 2;
      $wizard.find($('.progress-bar')).css({width: move_distance + '%'});


      $wizard.find($('.wizard-card .nav-pills li.active a .icon-circle')).addClass('checked');

    }
  });


  // Prepare the preview for profile picture
  /* $('#wizard-picture').change(function(){
     this.readURL(this);
   });*/
  /*
      $('[data-toggle="wizard-radio"]').click(function(){
        let wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', true);
      });*/

  /* $('[data-toggle="wizard-checkbox"]').click(function(){
     if ($(this).hasClass('active')) {
       $(this).removeClass('active');
       $(this).find('[type="checkbox"]').removeAttr('checked');
     } else {
       $(this).addClass('active');
       $(this).find('[type="checkbox"]').attr('checked', true);
     }
   });*/

  $('.set-full-height').css('height', 'auto');
}
refereshEntreprise(value) {
    this.entrepriseDetail = value ;
}refereshPersonne(value) {
    this.personneDetail = value;
}

}
