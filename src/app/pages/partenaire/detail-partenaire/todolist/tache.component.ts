import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import swal from 'sweetalert2';
import {AgendaService} from "../../../../service/agenda.service";

@Component({
  selector: 'app-tache',
  templateUrl: './tache.component.html',
  styleUrls: ['./tache.component.scss'],
  providers: [AgendaService]
})

export class TacheComponent implements OnInit  {
  todolist: Array<any> = [];
  newTaskText: string;
  isTache = false;


  constructor() { }

  ngOnInit() {

    this.todolist.forEach(item => {
      item.isOver = false;
      item.isEdit = false;
      item.editText = item.text;
    });
  }




  edit(index) {
    if (!this.todolist[index].isOver) {
      this.todolist[index].editText = this.todolist[index].text;
      this.todolist[index].isEdit = true;
    }
  }

  overMatter(index) {
    if (!this.todolist[index].isEdit) {
      this.todolist[index].isOver = !this.todolist[index].isOver;
    }
  }

  enterTaskEdit(index) {
    this.todolist[index].text = this.todolist[index].editText;
    this.todolist[index].isEdit = false;
  }

  cancelTaskEdit(index) {
    this.todolist[index].isEdit = false;
  }

  creatNewTask() {
    const newTask = new List;
    newTask.isEdit = false;
    newTask.isOver = false;
    newTask.text = this.newTaskText;
    this.todolist.unshift(newTask);
  }

  openModal(modal) {
    modal.open();
    console.log(this.isTache);
  }

  closeModal(modal) {
    modal.close();
  }

  onClose() {
    swal({
      type: 'success',
      title: 'Success!',
      text: ' fermer'
    });
  }


}
export class List {
  text: string;
  editText: string;
  isOver: boolean;
  isEdit: boolean;
}
