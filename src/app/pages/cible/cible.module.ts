import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CibleComponent } from './cible.component';
import { routing } from './cible.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxEchartsModule } from 'ngx-echarts';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EntrepriseComponent} from './components/entreprise/entreprise.component';
import {PersonneComponent} from './components/personne/personne.component';
import {MultipleSelectComponent} from './components/entreprise/multiple-select/multiple-select.component';
import {SelectModule} from 'ng2-select';
import {EChartsComponent} from './components/entreprise/echarts/echarts.component';
import {TodolistComponent} from './components/entreprise/todolist/todolist.component';
import {TodolistPerComponent} from './components/personne/todolistPer/todolistPer.component';
import {MultipleSelectPerComponent} from './components/personne/multiple-selectPer/multiple-selectPer.component';
import {EchartsPerComponent} from './components/personne/echartsPer/echartsPer.component';
import {ModalModule} from 'ngx-modal';
import {SharedFormCibleModule} from '../sharedFormCible/sharedFormModule';
import {PersonneDetailComponent} from './components/personne-detail/personne-detail.component';
import {OrderModule} from 'ngx-order-pipe';
import {PipeModule} from '../../shared/pipe/pipe.module';
import {EntrepriseDetailComponent} from './components/entreprise-detail/entreprise-detail.component';
import {RepresentantComponent} from './components/entreprise/representant/representant.component';
import {TodoService} from '../../service/todo';




@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxEchartsModule,
        routing,
        NgxPaginationModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule,
        ModalModule,
        SharedFormCibleModule,
        OrderModule,
        PipeModule,



    ],
    declarations: [
        CibleComponent,
        EntrepriseComponent,
        PersonneComponent,
        MultipleSelectComponent,
        MultipleSelectPerComponent,
        EChartsComponent,
        EchartsPerComponent,
        TodolistComponent,
        TodolistPerComponent,
        PersonneDetailComponent,
        EntrepriseDetailComponent,
        RepresentantComponent
    ],

    exports: [
      MultipleSelectComponent,
    ],
  providers: [TodoService]
})
export class CibleModule { }
