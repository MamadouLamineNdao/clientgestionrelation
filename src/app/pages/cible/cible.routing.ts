import { Routes, RouterModule } from '@angular/router';
import { CibleComponent } from './cible.component';
import {EntrepriseComponent} from './components/entreprise/entreprise.component';
import {PersonneComponent} from './components/personne/personne.component';

const childRoutes: Routes = [
    {
        path: '',
        component: CibleComponent,
        children: [
        { path: '', redirectTo: 'cible', pathMatch: 'full' },
        { path: 'personne', component: PersonneComponent },
        { path: 'entreprise', component: EntrepriseComponent },
      ]
    }
];

export const routing = RouterModule.forChild(childRoutes);
