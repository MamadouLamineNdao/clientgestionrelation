import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Personne} from '../../../../domain/personne';
import {CibleService} from '../../../../service/cible.service';
import swal from 'sweetalert2' ;
import {Intermediaire} from '../../../../domain/intermediaire';
import {Router} from '@angular/router';



@Component({
  selector: 'app-personne-detail',
  templateUrl: './personne-detail.component.html',
  styleUrls: ['./personne-detail.component.scss']
})
export class PersonneDetailComponent implements OnInit {

  @Input()
  personne: Personne ;
  @Input()
  personnes: Personne[] ;
  @Output() back: EventEmitter<Personne> = new EventEmitter() ;
  @Output() delete = new EventEmitter<number>() ;
  modal ;
  constructor(private cibleService: CibleService, private route: Router ) { }
  ngOnInit() {
  }
  openNewModal() {
    this.back.emit(this.personne) ;

  }


  onDelete() {
    console.log();
    swal({
      title: 'Etes vous sure?',
      text: 'Ce professionnel sera defintivement supprimé',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimez !',
      cancelButtonText: 'Non, annulez'

    }).then((result) => {
      if (result.value) {
        this.cibleService.deleteCible(this.personne).subscribe(
          () => {
            swal(
              'Supprimé!',
              'Suppression réussie.',
              'success'
            );
            for (let i = 0; i < this.personnes.length; i++) {
              if (this.personnes[i].idCible === this.personne.idCible) {
                this.personnes.splice(i, 1);
                break;
              }
            }
            this.delete.emit() ;

          },
          error =>
            swal(
              'Annulée',
              'Suppression annulée',
              'error'
            )
        ) ;
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Annulée',
          'Suppression annulée',
          'error'
        ) ;
      }
    });
  }
  onValidate() {
    let intermediaire: Intermediaire = new Intermediaire() ;
    intermediaire.dateDebutPartenariat = new Date().getTime() ;
    this.cibleService.validateCible2(intermediaire, this.personne.idCible).subscribe(
      part => {
        for (let i = 0; i < this.personnes.length; i++) {
          if (this.personnes[i].idCible === this.personne.idCible) {
            this.personnes.splice(i, 1);
            break;
          }
        }
        /* a enlever aprés */
        this.route.navigate(['pages/partenaire/intermediaire/detail', part.idPartenaire, 'personne']);
      }

      ,
      error => {
        console.log(error);
      }
    ) ;
  }


}
