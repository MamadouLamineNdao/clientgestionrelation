import { Component } from '@angular/core';

@Component({
  selector: 'app-multiplePer-select',
  templateUrl: './multiple-selectPer.component.html'
})
export class MultipleSelectPerComponent {
  public items: Array<string> = ['Developpement', 'Data Science', 'Intelligence artificielle', 'Réseaux informatiques', 'Télécommunication'];

  public value: any = ['Athens'];
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }

  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }
}
