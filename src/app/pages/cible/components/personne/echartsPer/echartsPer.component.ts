import { Component } from '@angular/core';
import {ChartsPerService} from './chartsPer.service';


@Component({
  selector: 'app-echartsPer',
  templateUrl: './echartsPer.component.html',
  styleUrls: ['./echartsPer.component.scss'],
  providers: [ChartsPerService]
})
export class EchartsPerComponent {
  showloading: boolean = false;
  BarOption;
  LineOption;
  PieOption;
  AnimationBarOption;

  constructor(private chartsService: ChartsPerService) {
    this.BarOption = this.chartsService.getBarOption();
    this.LineOption = this.chartsService.getLineOption();
    this.PieOption = this.chartsService.getPieOption();
    this.AnimationBarOption = this.chartsService.getAnimationBarOption();
  }
}
