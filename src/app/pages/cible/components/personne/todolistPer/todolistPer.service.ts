import { Injectable } from '@angular/core';

@Injectable()
export class TodolistPerService {

    private _todoList = [
        { text: 'Trouvez un prof de TechCom' },
        { text: 'Trouver un maintenancier pour le serveur'},
        { text: 'Trouver des traiteurs' },
        { text: 'Contacter Mr Dioum prof de Réseaux' },
        { text: 'Trouver un chauffeur' },
        { text: 'Trouver un comptable' },
    ];

    getTodoList() {
        return this._todoList;
    }
}
