import { Component, OnInit } from '@angular/core';
import {Personne} from '../../../../domain/personne';
import {CibleService} from '../../../../service/cible.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.scss']
})
export class PersonneComponent implements OnInit {

  personnes: Personne[];
  personne: Personne;
  modal ;
  order ;
  reverse;
  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  /*filtrage*/
  filtercolumn: string[] ;
  filter: string ;

  constructor(private cibleService: CibleService, private route: Router ) {
  }


  ngOnInit() {
    this.personne = new Personne();
    this.cibleService.showPersonnes().subscribe(
      data => {
        this.personnes = data;
      },
      error => {
        console.log('An error was occured.');
      }
    ) ;

  }
  /*Pagination du tableau*/
  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }


  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }
  /* methode pour les modals */
  openModal(modal: any, personne: Personne) {
    this.personne = personne;
    this.modal = modal;
    modal.open();
  }

  closeModal(modal) {
    modal.close();
  }
  closeModalAndNavigate(modal, id: number){
    this.closeModal(modal) ;
    this.route.navigate(['pages/partenaire/intermediaire/detail', id, 'personne']);
  }

  addPersonne(modal: any) {
    this.openModal(modal, new Personne());
  }

  closeParent(modal: any, personne: Personne) {
    this.modal.close();
    this.modal = modal;
    this.personne = personne;
    console.log("closeParent") ;
    console.log(this.personne) ;
    modal.open() ;
  }
  filterDefineColumn() {
    if (this.filter === null || this.filter === undefined || this.filter === '' ) {
      this.filtercolumn = [] ;
    }
    else {
      this.filtercolumn = [this.filter, 'nom', 'prenom', 'profession', 'telephone'] ;
    }
  }
}




