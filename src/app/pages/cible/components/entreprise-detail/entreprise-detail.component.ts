import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Entreprise} from '../../../../domain/entreprise';
import {CibleService} from '../../../../service/cible.service';
import swal from 'sweetalert2';
import {Intermediaire} from '../../../../domain/intermediaire';
import {Router} from '@angular/router';

@Component({
  selector: 'app-entreprise-detail',
  templateUrl: './entreprise-detail.component.html',
  styleUrls: ['./entreprise-detail.component.scss']
})
export class EntrepriseDetailComponent implements OnInit {
  @Input() entreprise: Entreprise;
  @Input() entreprises: Entreprise[];
  @Output() back: EventEmitter<Entreprise> = new EventEmitter();
  @Output() delete = new EventEmitter();


  constructor(private cibleService: CibleService, private route: Router) {
  }

  ngOnInit() {
  }
  openNewModal() {
    this.back.emit(this.entreprise) ;

  }
  onDelete() {
    console.log();
    swal({
      title: 'Etes vous sure?',
      text: 'Cet organisation sera defintivement supprimé',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimez le!',
      cancelButtonText: 'Non, annulez'

    }).then((result) => {
      if (result.value) {
        this.cibleService.deleteCible(this.entreprise).subscribe(
          () => {
            swal(
              'Supprimé!',
              'Suppression resussie',
              'success'
            );
            for (let i = 0; i < this.entreprises.length; i++) {
              if (this.entreprises[i].idCible === this.entreprise.idCible) {
                this.entreprises.splice(i, 1);
                break;
              }
            }
            this.delete.emit();

          },
          error =>
            swal(
              'Annulée',
              'Suppression annulée',
              'error'
            )
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Annulée',
          'Suppression annulée',
          'error'
        );
      }
    });
  }

  onValidate() {
    let intermediaire: Intermediaire = new Intermediaire();
    intermediaire.dateDebutPartenariat = new Date().getTime();
    console.log(intermediaire);
    this.cibleService.validateCible2(intermediaire, this.entreprise.idCible).subscribe(
      interm => {
        for (let i = 0; i < this.entreprises.length; i++) {
          if (this.entreprises[i].idCible === this.entreprise.idCible) {
            this.entreprises.splice(i, 1);
            break;
          }
        }
        this.delete.emit() ;
        this.route.navigate(['pages/partenaire/intermediaire/detail', interm.idPartenaire, 'entreprise']);
      },
      error => {
        console.log(error);
      }
    );

  }
}
