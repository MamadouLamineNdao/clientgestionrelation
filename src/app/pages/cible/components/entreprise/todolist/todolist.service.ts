import { Injectable } from '@angular/core';

@Injectable()
export class TodoListService {

    private _todoList = [
        { text: 'Etablir contact avec l\'incubateur DIAMOND' },
        { text: 'Besoin partenaire métier automobile'},
        { text: 'Vogage d\'étude pour 50 étudiants' },
        { text: 'Contact avec Start-up IA' },
        { text: 'Recherche Partenaire en Chine' },
        { text: 'Premier contact avec Tigo' },
    ];

    getTodoList() {
        return this._todoList;
    }
}
