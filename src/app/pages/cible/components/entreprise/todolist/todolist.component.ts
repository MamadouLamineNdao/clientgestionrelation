import {Component, Input, OnInit} from '@angular/core';
import { TodoListService } from './todolist.service';
import {TodoService} from '../../../../../service/todo';
import {Todo} from '../../../../../domain/todo';
@Component({
  selector: 'app-du-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss'],
  providers: [TodoListService]
})

export class TodolistComponent implements OnInit {

  // todolist: Array<any> = [];
  todolist: Array<Todo> = [];
  newTaskText: string;

  @Input() type: string

  constructor(private todoListService: TodoListService, private todoService: TodoService) { }

  ngOnInit() {
    /*this.todolist = this.todoListService.getTodoList();
    this.todolist.forEach(item => {
      item.isOver = false;
      item.isEdit = false;
      item.editText = item.text;
    });*/
    // this.type = 'Professionnel';
    this.todoService.getListTodoCible(this.type).subscribe(
      data => {
        this.todolist = data ;
        this.todolist.forEach(item => {
          item.isEdit = false;
        });
        this.todolist = this.todolist.reverse() ;
      }
    ) ;
  }

  edit(index) {
    if (!this.todolist[index].isOver) {
      this.todolist[index].editText = this.todolist[index].text;
      this.todolist[index].isEdit = true;
    }
  }

  overMatter(index) {
    if (!this.todolist[index].isEdit) {
      let copie: Todo = JSON.parse(JSON.stringify(this.todolist[index]));
      copie.isOver = !copie.isOver;
      this.todoService.updateTodo(copie).subscribe(
        data => {
          this.todolist[index].isOver = !this.todolist[index].isOver;
        }
      );
    }
  }

  enterTaskEdit(index) {

    let copie: Todo = JSON.parse(JSON.stringify(this.todolist[index]));
    copie.text = this.todolist[index].editText;
    this.todoService.updateTodo(copie).subscribe(
      data => {
        this.todolist[index].isEdit = false;
        this.todolist[index].text = this.todolist[index].editText;
      }
    );
  }

  cancelTaskEdit(index) {
    this.todolist[index].isEdit = false;
  }

  creatNewTask() {
    const newTask = new Todo();
    newTask.isEdit = false;
    newTask.isOver = false;
    newTask.type = this.type ;
    newTask.text = this.newTaskText;
    if ( this.newTaskText && this.newTaskText.length >= 1)
    {
      this.todoService.addTodo(newTask).subscribe(
        data => {
          this.todolist.unshift(data);
          this.newTaskText = '';
        }

      ) ;
    }



  }
  delete(index) {
    this.todoService.deleteTodo(this.todolist[index]).subscribe(
      () => {
        this.todolist.splice(index, 1 ) ;
    }
    );
  }
}
export class List {
  text: string;
  editText: string;
  isOver: boolean;
  isEdit: boolean;
}
