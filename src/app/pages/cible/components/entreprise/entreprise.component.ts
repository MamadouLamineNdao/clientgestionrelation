import { Component, OnInit } from '@angular/core';
import {Entreprise} from '../../../../domain/entreprise';
import {CibleService} from '../../../../service/cible.service';
import {Domaine} from '../../../../domain/domaine';
import {SousDomaine} from '../../../../domain/sous.domaine';
import {DomaineService} from '../../../../service/domaine.service';
import {Taille} from '../../../../domain/taille';

@Component({
  selector: 'app-entreprise',
  templateUrl: './entreprise.component.html',
  styleUrls: ['./entreprise.component.scss']
})

export class EntrepriseComponent implements OnInit {

  entreprises: Entreprise[];
  entreprise: Entreprise;
  domaines: Domaine[] ;
  domaine: Domaine ;
  sousDomaines: SousDomaine[] ;
  decor: string ;
  modal: any ;
  order ;
  reverse;
  sousDomainesCheck: number[] = [] ;
  filtercolumn: string[] ;
  filter: string ;
  taillemin = 'tout' ;
  taillemax = 'tout';
  tailleSelect = [] ;
  tailleSelectMin = [] ;
  tailleSelectMax = [] ;
  selectSoudomaine ;
  checkAllVar = true ;


  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;

  value: Array<any> = [] ;

  constructor(private cibleService: CibleService, private domaineService: DomaineService) {
    this.sousDomainesCheck = [] ;
  }

  ngOnInit() {
    this.domaine = new Domaine() ;
    this.entreprise = new Entreprise() ;
    this.chargerDomaine() ;
    /*charger tailleTab */
    this.cibleService.getAllTranches().subscribe(
      data => {
        const tailles: Taille[] = data ;
        for (const taille of tailles ) {
          this.tailleSelect.push(String(taille.min));
        }
        this.tailleSelectMin = JSON.parse(JSON.stringify(this.tailleSelect));
        this.tailleSelectMax = JSON.parse(JSON.stringify(this.tailleSelect));
        this.tailleSelectMax.splice(0, 1 ) ;
        this.tailleSelectMax.splice(this.tailleSelectMin.length - 1, 1) ;

      }
    ) ;

  }
  chargerDomaine() {
    this.domaineService.getAllDomaine().subscribe(
      data => {
        this.domaines = data;
        if (this.domaines !== null && this.domaines.length !== 0) {
          this.changeDecor(this.domaines[0]);
        }
      }
    ) ;
  }
  loadData() {

  }

  /*test*/
  clickFilter(sd, event) {
    if ( event.currentTarget.checked) {
      this.sousDomainesCheck.push(sd.idSousDomaine) ;
    }else {
      if (this.checkAllVar)
        this.sousDomainesCheck = [] ;
      else {
        for (let i = 0; i < this.sousDomainesCheck.length; i++) {
          if (this.sousDomainesCheck[i] === sd.idSousDomaine) {
            this.sousDomainesCheck.splice(i, 1);
            break;
          }
        }
      }
      this.checkAllVar = false ;

    }
    this.chargerEntreprise() ;

  }
  /*charger les entreprises séléectionnés*/
  chargerEntreprise() {
    this.cibleService.showEntreprises(this.TabNumberToString(this.sousDomainesCheck)).subscribe(
      entreps => {
        this.entreprises = entreps;
      },
      error1 => {console.log('An error was occured. getAlldomaine');
      }

    ) ;
  }
  getEntreprise() {
    if (this.taillemin === 'tout' && this.taillemax === 'tout')
      return this.entreprises ;
    if (this.taillemin !== 'tout' && this.taillemax !== 'tout' ) {
      const tabEntreprises: Entreprise[] = [] ;
      if ( this.entreprises !== null && this.entreprises !== undefined) {
        for ( const entreprise of this.entreprises) {
          if (entreprise.tailleEntreprise.min >= Number(this.taillemin) && entreprise.tailleEntreprise.max <= Number(this.taillemax)) {
            tabEntreprises.push(entreprise) ;
          }
        }
      }
      return tabEntreprises ;
    }
    if (this.taillemin !== 'tout' && this.taillemax === 'tout' ) {
      const tabEntreprises: Entreprise[] = [] ;
      if ( this.entreprises !== null && this.entreprises !== undefined) {
        for ( const entreprise of this.entreprises) {
          if (entreprise.tailleEntreprise.min >= Number(this.taillemin)) {
            tabEntreprises.push(entreprise) ;
          }
        }
      }
      return tabEntreprises ;
    }
    if (this.taillemin === 'tout' && this.taillemax !== 'tout' ) {
      const tabEntreprises: Entreprise[] = [] ;
      if ( this.entreprises !== null && this.entreprises !== undefined) {
        for ( const entreprise of this.entreprises) {
          if (entreprise.tailleEntreprise.max <= Number(this.taillemax)) {
            tabEntreprises.push(entreprise) ;
          }
        }
      }
      return tabEntreprises ;
    }

  }

  /*transformer le tableau de id n string pour l'envoyer par l'url*/
  TabNumberToString (tabNumber: number[]): string {
    let resultTab: string[] = [] ;
    for ( let id of tabNumber){
      resultTab.push(id.toString()) ;
    }
    return resultTab.join(',') ;

  }

  changeDecor(domaine) {
    if ( domaine !== undefined && domaine !== null && domaine.sousDomaines !== undefined) {
      this.decor = domaine.nomDomaine;
      this.domaine = domaine ;
      this.sousDomaines = this.domaine.sousDomaines ;
      this.sousDomainesCheck = [] ;
      for ( let sousDomaine of this.sousDomaines){
        this.sousDomainesCheck.push(sousDomaine.idSousDomaine) ;
      }
      this.chargerEntreprise();
    }

  }

  openModal(modal: any, entreprise: Entreprise) {
    this.entreprise = entreprise;
    this.modal = modal;
    modal.open();
  }

  closeModal(modal) {
    modal.close();
  }

  addEntreprise(modal: any) {
    this.value = [] ;
    this.domaine = undefined;
    this.openModal(modal, new Entreprise());
  }

  closeParent(modal: any, entreprise: Entreprise) {
    this.modal.close();
    this.modal = modal;
    this.entreprise = entreprise;
    this.domaine = undefined;
    this.value = [];
    modal.open() ;
    for (const sousDomaine of this.entreprise.sousDomaines) {
      const item = {
        id: sousDomaine.idSousDomaine,
        text: sousDomaine.libelle
      };
      this.value.push(item);
    }

  }
  /*Pagination du tableau*/
  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }


  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }
  filterDefineColumn() {
    if (this.filter === null || this.filter === undefined || this.filter === '' ) {
      this.filtercolumn = [] ;
    }else {
      this.filtercolumn = [this.filter, 'nomEntreprise', 'dateValidationSuggestion', 'sousDomaines', 'tailleEntreprise.min' ] ;
    }
  }

  checkAll(event) {
    this.checkAllVar = event.currentTarget.checked ;
    this.sousDomainesCheck = [] ;
    if (this.checkAllVar) {
      this.sousDomaines = this.domaine.sousDomaines ;
      for ( let sousDomaine of this.sousDomaines){
        this.sousDomainesCheck.push(sousDomaine.idSousDomaine) ;
      }
    }
    this.chargerEntreprise();
  }

}
