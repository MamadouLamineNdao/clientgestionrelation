import {Component, Input, OnInit} from '@angular/core';
import {Representant} from '../../../../../domain/representant';
import {Entreprise} from '../../../../../domain/entreprise';
import {RepresentantService} from '../../../../../service/representant';

@Component({
  selector: 'app-representant',
  templateUrl: './representant.component.html',
  styleUrls: ['./representant.component.scss']
})
export class RepresentantComponent implements OnInit {
representants: Representant[] ;
@Input()entreprise: Entreprise ;
  constructor(private representantService: RepresentantService) { }

  ngOnInit() {
  }

}
