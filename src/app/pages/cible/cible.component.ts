import { Component, OnInit } from '@angular/core';
import {CibleService} from '../../service/cible.service';

@Component({
  selector: 'app-cible',
  providers: [CibleService],
  template: `<router-outlet></router-outlet>`
})
export class CibleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
