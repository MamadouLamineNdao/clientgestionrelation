import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {LOCALE_ID, NgModule} from '@angular/core';
import { PagesModule } from './pages/pages.module';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {NgxLoadingModule} from 'ngx-loading';
import {DomaineService} from './service/domaine.service';
import {SousDomaineService} from './service/sousDomaine';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    PagesModule,
    HttpClientModule,
    NgxLoadingModule.forRoot({}),
    routing
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
    DomaineService,
    SousDomaineService,
    {provide: LOCALE_ID, useValue: 'fr-FR'}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
