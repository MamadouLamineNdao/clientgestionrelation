import { Component } from '@angular/core';
import {ProspectionStorageService} from './service/prospection.storage.service';
import {ProspectionService} from './service/prospection.service';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
  providers: [ProspectionStorageService]
})
export class AppComponent {
  title = 'app';
}
