import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'trieTable'
})

export class TrieTablePipe implements PipeTransform {
    transform(array: any[], args: string[]): any {
      if (array===undefined || array===null || array.length===0
          || args===undefined || args===null || args.length !==2){
        return array;
      }
      const field=args[0].split('.');
      const orderby=args[1];
      if (orderby.toLowerCase()!=='asc' && orderby.toLowerCase()!=='desc'){
        return array;
      }
      return this.sort(array, field, orderby);
    }
  sort(array: any[], fields: string[], orderby: string): any{
    array.sort((a: any, b: any) => {
      let value1: any;
      let value2: any;
      let field='';
      value1 = a;
      value2= b;
      if (typeof (value1) === 'object'){
        for (let i=0; i < fields.length; i++){
          if (value1!==undefined && value2!==undefined){
            value1 = value1[fields[i]];
            value2= value2[fields[i]];
            field=fields[i];
            if ((typeof value1) === 'number' || (typeof value1) === 'string'){
              break;
            }
          }
        }
      }
      if ((typeof value1) === 'number' && field.indexOf('date')===0){
        value1 = new Date(value1);
        value1 = value1.getTime();
        value2 = new Date(value2);
        value2 = value2.getTime();
      }
      if (value1!==undefined && value1!==null && value2!==undefined && value2!==null){
        if (orderby.toLowerCase()==='asc'){
          return value1.toString().toLowerCase().localeCompare(value2.toString().toLowerCase());
        }
        else {
          return value2.toString().toLowerCase().localeCompare(value1.toString().toLowerCase());
        }
      }
      else {
        return 0;
      }
    });
    return array;
  }
}
