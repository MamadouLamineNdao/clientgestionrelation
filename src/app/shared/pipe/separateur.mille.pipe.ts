import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'separateurMille'})
export class SeparateurMillePipe implements PipeTransform {
  transform(value: number): string {
    if (value === undefined || value === null){
      return '0';
    }
    const  tab = value.toString().split('');
    let result = '';
    let j = 1;
    for (let i = (tab.length - 1); i >= 0; i--){
      if (j === 4){
        result = tab[i] + ' ' + result;
        j = 2;
      }
      else {
        result = tab[i] + result;
        j++;
      }
    }
    return result;
  }
}
