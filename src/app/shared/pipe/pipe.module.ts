import {NgModule} from '@angular/core';
import {SeparateurMillePipe} from './separateur.mille.pipe';
import {FilterTablePipe} from './filter.table.pipe';
import {TrieTablePipe} from './trie.table.pipe';

@NgModule({
  declarations: [
    SeparateurMillePipe,
    FilterTablePipe,
    TrieTablePipe
  ],
  imports     : [],
  exports     : [
    SeparateurMillePipe,
    FilterTablePipe,
    TrieTablePipe
  ],
  providers: []
})

export class PipeModule {}
