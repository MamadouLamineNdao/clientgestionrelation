import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterTable'
})

export class FilterTablePipe implements PipeTransform {
  ok = false;
    transform(array: any[], args: string[]): any {
      const $this = this;
      if (array !== undefined && array !== null && args !== undefined && args !== null && args.length !== 0){
        const elements: any[] = [];
        Object.keys(array).forEach(function (key) {
          const obj = array[key];
          $this.ok = false;
          Object.keys(obj).forEach(function (key1) {
            if (!$this.ok && $this.subStringInArray(args, key1)){
              const value = obj[key1];
              if ((typeof value) === 'string'){
                if (value.toUpperCase().indexOf(args[0].toUpperCase()) !== -1){
                  elements.push(obj);
                  $this.ok = true;
                  return;
                }
              }
              else {
                if ((typeof value) === 'number'){
                  const val = value;
                  if (key1.indexOf('date')===0){
                    const date = $this.dateFormatFr(val);
                    if (date.indexOf(args[0].toUpperCase()) !== -1){
                      elements.push(obj);
                      $this.ok = true;
                      return;
                    }
                  }
                  else {
                    if (val.toString().toUpperCase().indexOf(args[0].toUpperCase()) !== -1){
                      elements.push(obj);
                      $this.ok = true;
                      return;
                    }
                  }
                }
                else {
                  if ((typeof value) === 'object'){
                    if (value!==null && value!==undefined && value.length!==undefined){
                      for (const v of value){
                        if ((typeof v) === 'string'){
                          if (v.toUpperCase().indexOf(args[0].toUpperCase()) !== -1){
                            elements.push(obj);
                            $this.ok = true;
                            return;
                          }
                        }
                      }
                    }
                    else {
                      $this.searchInObject(elements, obj, value, args, key1);
                    }
                  }
                }
              }
            }
          });
        });
        return elements;
      }
      else {
        return array;
      }
    }
  dateFormatFr(dateTime: any): any {
    const date = new Date(dateTime);
    const dd = date.getDate();
    let d = dd.toString();
    const mm = date.getMonth() + 1;
    let m = mm.toString();
    const yyyy = date.getFullYear();
    if(dd < 10) {
      d = '0' + dd;
    }
    if(mm < 10) {
      m = '0' + mm;
    }
    return (d + '/' + m + '/' + yyyy).toUpperCase();
  }
  subStringInArray(array: string[], sub:string): boolean{
      for (const el of array){
        if (el.indexOf(sub) === 0){
          return true;
        }
      }
      return false;
  }
  searchInObject(elements: any[], element: any,value: any, args: string[], key: string){
      const $this = this;
    if (!$this.ok && value !== undefined && value !== null && value.length === undefined){
      let find = false;
      Object.keys(value).forEach(function (k) {
        const val = value[k];
        if (!find && args.includes(key + '.' + k)){
          if ((typeof val) === 'string'){
            if (val.toUpperCase().indexOf(args[0].toUpperCase()) !== -1){
              find =true;
              elements.push(element);
              $this.ok=true;
              return;
            }
          }
          if ((typeof val) === 'number'){
            if (k.indexOf('date')===0){
              const date = $this.dateFormatFr(val);
              if (date.indexOf(args[0].toUpperCase()) !== -1){
                find =true;
                elements.push(element);
                $this.ok=true;
                return;
              }
            }
            else {
              if (val.toString().toUpperCase().indexOf(args[0].toUpperCase()) !== -1){
                find =true;
                elements.push(element);
                $this.ok=true;
                return;
              }
            }
          }
        }
        else {
          if (!$this.ok && (typeof val) === 'object' && $this.subStringInArray(args, key + '.' + k)){
            if (val!==null && val!==undefined && val.length!==undefined){
              for (const v of value){
                if ((typeof v) === 'string'){
                  if (v.toUpperCase().indexOf(args[0].toUpperCase()) !== -1){
                    find =true;
                    elements.push(element);
                    $this.ok=true;
                    return;
                  }
                }
              }
            }
            else {
              return $this.searchInObject(elements, element, val, args, key + '.' + k);
            }
          }
          else {
            return;
          }
        }
      });
    }
  }
}
