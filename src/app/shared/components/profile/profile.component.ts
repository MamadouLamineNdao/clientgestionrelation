import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  avatarImgSrc: string = 'assets/images/nao.jpg';
  userName: string = 'PESI';
  userPost: string = 'Détecteur de potentiel partenaire';
  
  constructor() { }

  ngOnInit() {
  }

}
