import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'pages/cible/entreprise',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'pages/cible/personne'
  },


];

export const routing = RouterModule.forRoot(appRoutes);
